;;;
;;; Test codecheck
;;;

(use gauche.test)

(test-start "codecheck")
(use codecheck)
(use srfi-13)
(test-module 'codecheck)

(define make-sandbox
  (let ((counter 0))
    (lambda ()
      (inc! counter)
      (make-module (string->symbol (format "sandbox~d" counter))))))

(define-syntax test-codecheck
  (syntax-rules ()
    ((_ desc expected body ...)
     (test desc expected (lambda ()
                           ((with-module codecheck check-list-clear!))
                           (with-module codecheck body ...))))))

(define-macro (test-gref-check desc expected . bodies)
  `(test ,desc (if (string-null? ,expected)
                   ""
                   (format "~a referenced but not defined\n" ,expected))
         (lambda ()
           (eval '(begin
                    ((with-module codecheck check-list-clear!))
                    (regexp-replace* (with-output-to-string
                                      (lambda ()
                                        (with-module codecheck
                                          (let ((env (make-initial-env)))
                                            (for-each (cut scan-expr env <>) ',bodies)
                                            (do-check)))))
                                     #/^[^:]+:[^:]+: error: /
                                     ""
                                     #/sandbox\d+/
                                     "sandbox"))
                 (make-sandbox)))))

(define-macro (test-err-check desc expected . bodies)
  `(test ,desc ,expected
         (lambda ()
           (eval '(begin
                    ((with-module codecheck check-list-clear!))
                    (string-join (map (cut regexp-replace* <>
                                           #/^[^:]+:[^:]+: [^:]+: /
                                           ""
                                           #/sandbox\d+/
                                           "sandbox")
                                      (string-split (with-output-to-string
                                                      (lambda ()
                                                        (with-module codecheck
                                                          (let ((env (make-initial-env)))
                                                            (for-each (cut scan-expr env <>)
                                                                      ',bodies)
                                                            (do-check)))))
                                                    "\n"))
                                 "\n"))
                 (make-sandbox)))))

(test-err-check
 "scan-expr (gref: the symbol exists)"
 ""
 (define foo 1)
 foo)

(test-err-check
 "scan-expr (gref: the symbol doesn't exists)"
 "bar(sandbox) referenced but not defined\n"
 (define foo 1)
 bar)

(test-err-check
 "scan-expr (gref: the symbol exists in gauche module)"
 ""
 print)


(test-gref-check
 "scan-expr:define body (gref: the symbol doesn't exists)"
 "bar(sandbox)"
 (define foo bar))

(test-gref-check
 "scan-expr:define body (gref: the symbol exists)"
 ""
 (define foo print))

(test-gref-check
 "scan-expr:lset body (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (let ((v 1))
   (set! v foo)
   v))

(test-gref-check
 "scan-expr:lset body (gref: the symbol exists)"
 ""
 (define foo 2)
 (let ((v 1))
   (set! v foo)
   v))

(test-gref-check
 "scan-expr:if expr (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (if foo #t #f))

(test-gref-check
 "scan-expr:if expr (gref: the symbol exists)"
 ""
 (define foo 2)
 (if foo #t #f))

(test-gref-check
 "scan-expr:if then (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (if (read) foo #f))

(test-gref-check
 "scan-expr:if then (gref: the symbol exists)"
 ""
 (define foo 2)
 (if (read) foo #f))

(test-gref-check
 "scan-expr:if else (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (if (read) #f foo))

(test-gref-check
 "scan-expr:if else (gref: the symbol exists)"
 ""
 (define foo 2)
 (if (read) #f foo))

(test-gref-check
 "scan-expr:let lvar (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (let ((v foo))
   v))

(test-gref-check
 "scan-expr:if lvar (gref: the symbol exists)"
 ""
 (define foo 2)
 (let ((v foo))
   v))

(test-gref-check
 "scan-expr:let body (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (let ((v 1))
   foo
   v))

(test-gref-check
 "scan-expr:let body (gref: the symbol exists)"
 ""
 (define foo 2)
 (let ((v 1))
   foo
   v))

(test-gref-check
 "scan-expr:receive expr (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (receive (a b c) (values foo 2 3)
   a b c))

(test-gref-check
 "scan-expr:receive body (gref: the symbol exists)"
 ""
 (define foo 2)
 (receive (a b c) (values 1 2 3)
   foo b c))

(test-gref-check
 "scan-expr:begin (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (begin 1 foo))

(test-gref-check
 "scan-expr:begin (gref: the symbol exists)"
 ""
 (define foo 2)
 (begin 1 foo))

(test-gref-check
 "scan-expr:call proc (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (foo 1))

(test-gref-check
 "scan-expr:call proc (gref: the symbol exists)"
 ""
 (define foo print)
 (foo 1))

(test-gref-check
 "scan-expr:call args (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (print foo))

(test-gref-check
 "scan-expr:call args (gref: the symbol exists)"
 ""
 (define foo 2)
 (print foo))

(test-gref-check
 "scan-expr:inline asm args (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 (+ foo 1))

(test-gref-check
 "scan-expr:inline asm args (gref: the symbol exists)"
 ""
 (define foo 2)
 (+ foo 1))

(test-gref-check
 "scan-expr:quasiquote cons (gref: the symbol of car doesn't exists)"
 "foo(sandbox)"
 `(,foo . 1))

(test-gref-check
 "scan-expr:quasiquote cons (gref: the symbol of car exists)"
 ""
 (define foo 2)
 `(,foo . 1))

(test-gref-check
 "scan-expr:quasiquote cons (gref: the symbol of cdr doesn't exists)"
 "foo(sandbox)"
 `(1 . ,foo))

(test-gref-check
 "scan-expr:quasiquote cons (gref: the symbol of cdr exists)"
 ""
 (define foo 2)
 `(1 . ,foo))

(test-gref-check
 "scan-expr:quasiquote append (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 `(,@foo . 1))

(test-gref-check
 "scan-expr:quasiquote append (gref: the symbol exists)"
 ""
 (define foo '(2))
 `(,@foo . 1))

(test-gref-check
 "scan-expr:quasiquote vector (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 `#(,foo 1))

(test-gref-check
 "scan-expr:quasiquote vector (gref: the symbol exists)"
 ""
 (define foo 2)
 `#(,foo 1))

(test-gref-check
 "scan-expr:quasiquote list->vector (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 `#(1 ,@foo))

(test-gref-check
 "scan-expr:quasiquote list->vector (gref: the symbol exists)"
 ""
 (define foo 2)
 `#(1 ,@foo))

(test-gref-check
 "scan-expr:quasiquote list (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 `(1 ,foo))

(test-gref-check
 "scan-expr:quasiquote list (gref: the symbol exists)"
 ""
 (define foo 2)
 `(1 ,foo))

(test-gref-check
 "scan-expr:quasiquote list* (gref: the symbol doesn't exists)"
 "foo(sandbox)"
 `(1 2 . ,foo))

(test-gref-check
 "scan-expr:quasiquote list* (gref: the symbol exists)"
 ""
 (define foo 2)
 `(1 2 . ,foo))

(test-gref-check
 "scan-expr:$memv, $eq?, $eqv scan"
 ""
 (define a 1)
 (case a ((1 eins) 'one) ((2) 'two) ((drei) 'three)))

(test-err-check
 "scan-expr:arity check is ok"
 ""
 (string-length "foo"))

(test-err-check
 "scan-expr:arity check (arity is integer and the number of arguments is lesser than arity)"
 "wrong number of arguments: #<subr string-length> requires 1, but got 0\n"
 (string-length))

(test-err-check
 "scan-expr:arity check (arity is integer and the number of arguments is greater than arity)"
 "wrong number of arguments: #<subr string-length> requires 1, but got 2\n"
 (string-length "foo" "bar"))

(test-err-check
 "scan-expr:arg type check"
 "wrong type for argument 1 of #<subr string-length>: <string> required, but got #<subr +>\n"
 (string-length +))

(test-err-check
 "scan-expr:return type check"
 "wrong type for argument 1 of #<subr string-length>: <string> required, but got #<an instance of <integer>>\n"
 (string-length (string-length "abc")))

(test-err-check
 "scan-expr:arity check (arity is arity-at-least and the number of argument is lesser than arity)"
 "wrong number of arguments: #<subr make-vector> requires more than 1, but got 0\n"
 (make-vector))

(test-err-check
 "scan-expr:arity check (arity is list and the number of argument is lesser than arity)"
 "#<generic arity-check-test-method> can't be applied with arguments (1)\n"
 (define-method arity-check-test-method (a b c)
   #t)
 (define-method arity-check-test-method (a b)
   #t)
 (arity-check-test-method 1))

(test-err-check
 "scan-expr:arity check (arity is list and the number of argument is greater than arity)"
 "#<generic arity-check-test-method> can't be applied with arguments (1 2 3 4)\n"
 (define-method arity-check-test-method (a b c)
   #t)
 (define-method arity-check-test-method (a b)
   #t) 
 (arity-check-test-method 1 2 3 4))

(test-err-check
 "scan-expr:arity check OK (lambda)"
 ""
 (let ((f (lambda (x y z) (list x y z))))
   (f 1 2 3)))

(test-err-check
 "scan-expr:arity check NG (lambda)"
 "wrong number of arguments: #<closure f> requires 3, but got 2\n"
 (let ((f (lambda (x y z) (list x y z))))
   (f 1 2)))

(test-err-check
 "scan-expr:arity check NG (lambda)"
 "wrong number of arguments: #<closure f> requires 3, but got 4\n"
 (let ((f (lambda (x y z) (list x y z))))
   (f 1 2 3 4)))

(test-err-check
 "scan-expr:arity check OK (lambda)"
 ""
 (let ((f (lambda (x y . z) (list x y z))))
   (f 1 2)))

(test-err-check
 "scan-expr:arity check OK (lambda)"
 ""
 (let ((f (lambda (x y . z) (list x y z))))
   (f 1 2 3 4)))

(test-err-check
 "scan-expr:arity check NG (lambda)"
 "wrong number of arguments: #<closure f> requires more than 2, but got 1\n"
 (let ((f (lambda (x y . z) (list x y z))))
   (f 1)))

(test-err-check
 "scan-expr:arity check OK (define)"
 ""
 (define (f x) x)
 (f 1))

(test-err-check
 "scan-expr:arity check NG (define)"
 "wrong number of arguments: #<closure f> requires 1, but got 2\n"
 (define (f x) x)
 (f 1 2))

(test-err-check
 "scan-expr:arity check OK (local variable)"
 ""
 (let ((f (lambda (x) x)))
   (f 1)))

(test-err-check
 "scan-expr:arity check NG (local variable)"
 "wrong number of arguments: #<closure f> requires 1, but got 2\n"
 (let ((f (lambda (x) x)))
   (f 1 2)))

(test-err-check
 "scan-expr:the continuation expected one value (define)"
 "the continuation expects one value, but got 3 values\n"
 (define a (values 1 2 3)))

(test-err-check
 "scan-expr:the continuation expected one value (proc)"
 "the continuation expects one value, but got 2 values\n"
 ((values (lambda (x) x) 1) 1))

(test-err-check
 "scan-expr:the continuation expected one value (args)"
 "the continuation expects one value, but got 2 values\n"
 (print (values 1 2)))

(test-err-check
 "scan-expr:received fewer values than expected"
 "reference of values index out of range: 2 (got 2 values)\nexpected 3 values, but got 2 values\n"
 (receive (a b c) (values 1 2)
   (list a b c)))

(test-err-check
 "scan-expr:received more values than expected"
 "expected 3 values, but got 4 values\n"
 (receive (a b c) (values 1 2 3 4)
   (list a b c)))

(test-err-check
 "scan-expr:received 0 values and got 0 values"
 ""
 (receive () (values)
   (print "OK")))

(test-err-check
 "scan-expr:received 3 values and got 3 values"
 ""
 (receive (a b c) (values 1 2 3)
   (list a b c)))

(test-err-check
 "scan-expr:received more than 3 values and got 3 values"
 ""
 (receive (a b . c) (values 1 2 3)
   (list a b c)))

(test-err-check
 "scan-expr:received more than 3 values and got 4 values"
 ""
 (receive (a b . c) (values 1 2 3 4)
   (list a b c)))

(test-err-check
 "scan-expr:received value's type"
 "wrong type for argument 1 of #<subr string-length>: <string> required, but got 1\n"
 (receive (a b) (values "a" 1)
   (string-length b)))

(test-err-check
 "scan-expr:check lambda arg type OK"
 ""
 (define (make-csv-record vals)
   (string-join vals ","))
 (make-csv-record '("1" "2" "3")))

(test-err-check
 "scan-expr:check lambda arg type NG"
 "wrong type for argument 1 of #<subr string-join>: <list> required, but got \"123\"\n"
 (define (make-csv-record vals)
   (string-join vals ","))
 (make-csv-record "123"))

(test-err-check
 "scan-expr:check lambda return type OK"
 ""
 (define (make-csv-record vals)
   (string-join vals ","))
 (string-length (make-csv-record '("1" "2" "3"))))

(test-err-check
 "scan-expr:check value-lambda is procedure"
 ""
 (define (make-csv-record vals)
   (string-join vals ","))
 (pa$ make-csv-record '("1" "2" "3")))

(test-err-check
 "scan-expr:check procedure"
 ""
 (pa$ car))

(test-err-check
 "scan-expr:check generic"
 ""
 (pa$ ref))

(test-err-check
 "scan-expr:check object-apply"
 ""
 (pa$ #/aaa/))

(test-err-check
 "scan-expr:check object-apply 2"
 ""
 (define-method object-apply ((a <integer>))
   a)
 (pa$ 1))

(test-err-check
 "scan-expr:check lambda arg type NG"
 "wrong type for argument 1 for #<closure pa$>: an applicable object required, but got #<an instance of <string>>\n"
 (define (make-csv-record vals)
   (string-join vals ","))
 (pa$ (make-csv-record '("1" "2" "3"))))

(test-err-check
 "scan-expr:infinite loop"
 ""
 (let loop ((x 1))
   (loop 2)))

(test-err-check
 "scan-expr:infinite loop 2"
 ""
 (define (f) (g))
 (define (g) (f))
 (f))

(test-err-check
 "scan-expr:wrong type in recursive call"
 "wrong type for argument 1 of #<subr vector-ref>: <vector> required, but got 1\n"
 (let loop ((vec #(1 2 3))
            (x 1))
   (print (vector-ref vec x))
   (loop x vec)))

(test-err-check
 "scan-expr:check whether value-lambda's is-running? flag is reset collectly."
 "wrong type for argument 1 of #<subr port-closed?>: <port> required, but got #<an instance of <string>>\n"
 (define (make-csv-record vals)
   (string-join vals ","))
 (string-length (make-csv-record '("1" "2" "3"))) ;OK
 (port-closed? (make-csv-record '("1" "2" "3")))  ;NG
 )

(test-err-check
 "scan-expr:force's arity check OK."
 ""
 (force 1))

(test-err-check
 "scan-expr:force's arity check NG."
 "wrong number of arguments: #<subr force> requires 1, but got 2\n"
 (force 1 2))

(test-err-check
 "scan-expr:force's return type (not promise)"
 "wrong type for argument 1 of #<subr string-length>: <string> required, but got 1\n"
 (string-length (force 1)))

(test-err-check
 "scan-expr:force's return type (promise)"
 "wrong type for argument 1 of #<subr string-length>: <string> required, but got 1\n"
 (define a 1)
 (string-length (force (delay a))))

(test-err-check
 "scan-expr: + check OK"
 ""
 (let ((a 1))
   (+ a 2)))

(test-err-check
 "scan-expr: + check NG"
 "#<generic object-+ (0)> can't be applied with arguments (\"a\" 2)\n"
 (let ((a "a"))
   (+ a 2)))

(test-err-check
 "scan-expr: + check (defines object-+)"
 ""
 (define-method object-+ ((a <string>) b)
   (string-append a (x->string b)))
 (let ((a "a"))
   (+ a 2)))

(test-err-check
 "scan-expr:define-class OK"
 ""
 (define-class <my-order> (<sequence>)
   ())
 (sort (make <my-order>)))

(test-err-check
 "scan-expr:define-class OK"
 ""
 (define-class <my-order> ()
   ())
 (boolean (make <my-order>)))

(test-err-check
 "scan-expr:define-class NG 1"
 "wrong type for argument 1 of #<closure port-tell>: <port> required, but got #<an instance of <my-order>>\n"
 (define-class <my-order> (<sequence>)
   ())
 (port-tell (make <my-order>)))

(test-err-check
 "scan-expr:define-class NG 2"
 "wrong type for argument 1 of #<closure sort>: <sequence> required, but got #<an instance of <my-order>>\n"
 (define-class <my-order> ()
   ())
 (sort (make <my-order>)))

(test-err-check
 "scan-expr:define-class OK (specified a metaclass)"
 ""
 (define-class <my-order-meta> (<class>)
   ())
 (define-class <my-order> (<class> <sequence>)
   ()
   :metaclass <my-order-meta>)
 (sort (make <my-order>)))

(test-err-check
 "scan-expr:define-class NG (specified a metaclass)"
 "wrong type for argument 1 of #<closure sort>: <sequence> required, but got #<an instance of <my-order>>\n"
 (define-class <my-order-meta> (<class>)
   ())
 (define-class <my-order> ()
   ()
   :metaclass <my-order-meta>)
 (sort (make <my-order>)))

(test-err-check
 "scan-expr:use a global reference before it defines"
 "bar(sandbox) referenced but not defined\n"
 bar
 (define bar 1))

(test-err-check
 ""
 ""
 (define (mag a b c) 1)
 (define (unit-vector x y z)
   (let ((d (mag x y z)))
     (values (/ x d) (/ y d) (/ z d)))))

(test-err-check
 "infinite loop check"
 ""
 (let ((a 1))
   (let ((a a))
     a)))

(test-err-check
 "infine loop check"
 ""
 (define (foo a)
   (foo a)
   (foo a))
 (foo 1))

(test-err-check
 ""
 ""
 (define (kahua-write-static-file path nodes context . rargs)
   (when (string-scan path "../")
     (error "can't use 'up' component in kahua-write-static-file" path))))

(test-err-check
 ""
 ""
 (use srfi-11)
 (let*-values (((port str) (sys-mkstemp "foo")))
   (close-output-port port)))

(test-err-check
 ""
 ""
 (cons (delay 1) (delay 2)))

(test-err-check
 "conditional branch (expr is #f)"
 ""
 (define (foo v)
   (let ((lst '(1 2 3)))
     (if v
         (list-ref lst v)
         #f)))
 (foo #f))

(test-err-check
 "conditional branch (expr is not #f)"
 ""
 (define (foo v)
   (let ((lst '(1 2 3)))
     (if v
         (list-ref lst v)
         (string-length v))))
 (foo 1))

(test-err-check
 "multiple arg types"
 ""
 (vector-ref #(1 2 3) (min 1 2 3)))

(test-err-check
 "define method 1"
 "wrong type for argument 2 of #<subr vector-ref>: <integer> required, but got #<an instance of <string>>\n"
 (define-method foo ((a <integer>) b)
   (+ a b))
 (define-method foo ((a <string>) b)
   (string-append a b))
 (vector-ref #(1 2 3) (foo "a" "b")))

(test-err-check
 "define method 2"
 ""
 (define-method foo ((a <integer>) b)
   (+ a b))
 (define-method foo ((a <string>) b)
   (string-append a b))
 (vector-ref #(1 2 3) (foo 1 1)))

(test-err-check
 "check unbound variable in define-method"
 "c(sandbox) referenced but not defined\n"
 (define-method foo ((a <integer>) b)
   (+ a b c)))

(test-err-check
 "accessor 1"
 "#<generic a-of> can't be applied with arguments (\"aaa\")\n"
 (define-class <foo> ()
   ((a :init-value 1
       :accessor a-of)))
 (define-class <bar> ()
   ((a :init-value 1
       :accessor a-of)))
 (a-of (make <foo>))
 (a-of (make <bar>))
 (a-of "aaa"))

(test-err-check
 "optional argument 1"
 ""
 (atan 1))

(test-err-check
 "optional argument 2"
 ""
 (atan 1.0 2.0))

(test-err-check
 "optional argument 3"
 "wrong type for argument 2 for #<closure atan>: <number> required, but got \"a\"\n"
 (atan 1.0 "a"))

(test-err-check
 "object-apply"
 ""
 (#/aaa/ "aaa"))

(test-err-check
 "object-apply NG"
 "#<generic object-apply (9)> can't be applied with arguments (1 \"aaa\")\n"
 (1 "aaa"))

(test-err-check
 "(load :paths '(\"test\"))"
 "wrong type for argument 2 for #<closure atan>: <number> required, but got \"a\"\n"
 (load "err.scm" :paths '("test")))

(test-err-check
 "(load :paths #f)"
 "wrong type for argument 2 for #<closure atan>: <number> required, but got \"a\"\n"
 (load "./test/err.scm" :paths #f))

(test-err-check
 "wrong keyword"
 "wrong keyword :foo for #<closure load>\n"
 (load "foo" :foo #t))

(test-err-check
 "no value is specified"
 "no value is specified for :paths\n"
 (load "foo" :paths))

(test-err-check
 "append 1"
 ""
 (length (append '(1 2 3) '(4 5 6))))

(test-err-check
 "append 2"
 ""
 (car (append '(1 2 3) 4)))

(test-err-check
 "append 3"
 "#<subr append> can't be applied with arguments (1 2)\n"
 (car (append 1 2)))

(test-err-check
 "call-with-output-file"
 ""
 (let ((buf (make-keyword 'full)))
   (call-with-output-file "foo"
     (lambda (port)
       #f)
     :buffering buf)))

(test-err-check
 "macro uses a procedure which is defined in the top level."
 ""
 (define (my-func)
   #t)
 (define-macro (my-macro expr)
   `(begin
      ,expr
      ,(my-func)))
 (my-macro (+ 1 1)))

(test-err-check
 "check a constant object at a method argument"
 ""
 (define-class <point> ()
   ((x :accessor x-of
       :init-keyword :x)))
 (define-constant eye (make <point> :x 0))
 (define (f)
   (x-of eye)))

;; epilogue
(test-end)
