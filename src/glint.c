/**
 *  glint.c
 *  
 *   Copyright (c) 2008 KOGURO, Naoki (naoki@koguro.net)
 *  
 *   Permission is hereby granted, free of charge, to any person 
 *   obtaining a copy of this software and associated 
 *   documentation files (the "Software"), to deal in the 
 *   Software without restriction, including without limitation 
 *   the rights to use, copy, modify, merge, publish, distribute, 
 *   sublicense, and/or sell copies of the Software, and to 
 *   permit persons to whom the Software is furnished to do so, 
 *   subject to the following conditions:
 *  
 *   The above copyright notice and this permission notice shall 
 *   be included in all copies or substantial portions of the 
 *   Software.
 *  
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
 *   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
 *   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 *   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS 
 *   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 *   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 *   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 *   $Id: $
 */

#define GAUCHE_API_0_9        /* temporary compatibility stuff */
#include "gauche.h"

#include <signal.h>

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif
#ifdef MSVC
int optind = 1;
char *optarg = NULL;
int getopt(int argc, char **argv, const char *spec);
#endif /*MSVC*/

ScmObj load_paths = SCM_NIL;

void usage(void)
{
    fprintf(stderr,
            "Usage: glint [-I<path>] [file]\n"
            "options:\n"
            "  -I<path> Adds <path> to the head of the load path list.\n"
            );
    exit(1);
}

int parse_options(int argc, char *argv[])
{
    int c;
    while ((c = getopt(argc, argv, "I:")) >= 0) {
        switch (c) {
        case 'I':
            load_paths = Scm_Cons(SCM_MAKE_STR_COPYING(optarg), load_paths);
            break;
        case '?':
            usage();
            break;
        }
    }
    return optind;
}

static void sig_setup(void)
{
    sigset_t set;
    sigfillset(&set);
    sigdelset(&set, SIGABRT);
    sigdelset(&set, SIGILL);
#ifdef SIGKILL
    sigdelset(&set, SIGKILL);
#endif
#ifdef SIGCONT
    sigdelset(&set, SIGCONT);
#endif
#ifdef SIGSTOP
    sigdelset(&set, SIGSTOP);
#endif
    sigdelset(&set, SIGSEGV);
#ifdef SIGBUS
    sigdelset(&set, SIGBUS);
#endif /*SIGBUS*/
#if defined(GC_LINUX_THREADS)
    /* some signals are used in the system */
    sigdelset(&set, SIGPWR);  /* used in gc */
    sigdelset(&set, SIGXCPU); /* used in gc */
    sigdelset(&set, SIGUSR1); /* used in linux threads */
    sigdelset(&set, SIGUSR2); /* used in linux threads */
#endif /*GC_LINUX_THREADS*/
#if defined(GC_FREEBSD_THREADS)
    sigdelset(&set, SIGUSR1); /* used by GC to stop the world */
    sigdelset(&set, SIGUSR2); /* used by GC to restart the world */
#endif /*GC_FREEBSD_THREADS*/
    Scm_SetMasterSigmask(&set);
}

/* Load gauche-init.scm */
void load_gauche_init(void)
{
    ScmLoadPacket lpak;
    if (Scm_Load("gauche-init.scm", 0, &lpak) < 0) {
        Scm_Printf(SCM_CURERR, "gosh: WARNING: Error while loading initialization file: %A(%A).\n",
                   Scm_ConditionMessage(lpak.exception),
                   Scm_ConditionTypeName(lpak.exception));
    }
}

/* Error handling */
void error_exit(ScmObj c)
{
    ScmObj m = Scm_ConditionMessage(c);
    if (SCM_FALSEP(m)) {
        Scm_Printf(SCM_CURERR, "glint: Thrown unknown condition: %S\n", c);
    } else {
        Scm_Printf(SCM_CURERR, "glint: %S: %A\n", Scm_ConditionTypeName(c), m);
    }
    Scm_Exit(1);
}

/*-----------------------------------------------------------------
 * MAIN
 */
int main(int argc, char **argv)
{
    int argind;
    ScmObj cp;
    ScmObj check_files = SCM_NIL;
    int exit_code = 0;
    ScmObj mainproc;
    ScmLoadPacket lpak;

    GC_INIT();
    Scm_Init(GAUCHE_SIGNATURE);
    sig_setup();

    argind = parse_options(argc, argv);

    /* load init file */
    load_gauche_init();

    if (optind < argc) {
        int i;
        for (i = optind; i < argc; ++i) {
            check_files = Scm_Cons(SCM_MAKE_STR_IMMUTABLE(argv[i]), check_files);
        }
    }
    SCM_DEFINE(Scm_UserModule(), "*argv*", SCM_LIST1(SCM_MAKE_STR_IMMUTABLE("")));
    SCM_DEFINE(Scm_UserModule(), "*program-name*", SCM_MAKE_STR_IMMUTABLE(""));

    SCM_FOR_EACH(cp, Scm_Reverse(load_paths)) {
        Scm_AddLoadPath(Scm_GetStringConst(SCM_STRING(SCM_CAR(cp))), FALSE);
    }

    if (Scm_Load("codecheck", 0, &lpak) < 0) {
        error_exit(lpak.exception);
    }

    mainproc = Scm_SymbolValue(SCM_FIND_MODULE("codecheck", 0),
                               SCM_SYMBOL(SCM_INTERN("main")));
    if (SCM_PROCEDUREP(mainproc)) {
        ScmObj r = Scm_ApplyRec(mainproc, SCM_LIST1(Scm_Reverse(check_files)));
        if (SCM_INTP(r)) {
            exit_code = SCM_INT_VALUE(r);
        } else {
            exit_code = 70;
        }
    } else {
        Scm_Error("codecheck module isn't installed properly.");
    }

    /* All is done. */
    Scm_Exit(exit_code);
    return 0;
}

#if defined(MSVC)
/* getopt emulation.  this is NOT a complete implementation of getopt;
   we know how it is used in main.c, so we don't need full-spec. */
int getopt(int argc, char **argv, const char *spec)
{
    static int clusterind = 0;

    char optchar, *optspec;
    
    do {
        if (optind >= argc) return -1;
        if (clusterind > 0) {
            /* remaining options */
            if (argv[optind][clusterind] == '\0') {
                clusterind = 0;
                optind++;
                continue;
            } else {
                optchar = argv[optind][clusterind];
            }
        } else {
            if (argv[optind][0] != '-') return -1;
            if (argv[optind][1] == '\0'
                || (argv[optind][1] == '-' && argv[optind][2] == '\0')) {
                return -1;
            }
            /* found an option. */
            optchar = argv[optind][1];
            clusterind = 2;
        }

        /* now we have option char in optchar.  we know spec[0] == '+' so
           we skip it. */
        if ((optspec = strchr(spec+1, optchar)) == NULL) return '?';
        
        if (optspec[1] != ':') return optchar;

        /* look for the argument. */
        if (argv[optind][clusterind] == '\0') {
            if (optind == argc-1) {
                optarg = NULL;
                return '?';
            } else {
                optarg = argv[optind];
                optind++;
                clusterind = 0;
                return optchar;
            }
        } else {
            optarg = &(argv[optind][clusterind]);
            optind++;
            clusterind = 0;
            return optchar;
        }
    } while (0);
    return -1;
}
#endif /*MSVC*/
