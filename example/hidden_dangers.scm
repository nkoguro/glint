(define (test a b)
  (print (+ a b)))

(define (main args)
  (if (< 3 (length args))
      (test 1 test)
      (test 1 2))
  0)
