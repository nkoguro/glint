;;;
;;; codecheck
;;;

(define-module codecheck
  (use gauche.parameter)
  (use gauche.sequence)
  (use util.match)
  (use gauche.vm.insn)

  (use codecheck.environment)
  (use codecheck.value)
  (use codecheck.util)
  (use util.isomorph)
  (use srfi-14)
  (use util.queue)
  (use srfi-1)
  )

(select-module codecheck)

;; check-list
(define check-list (make-parameter (make-queue)))

(define (check-list-add! checker)
  (enqueue! (check-list) checker))

(define (do-check)
  (until (queue-empty? (check-list))
    ((dequeue! (check-list)))))

(define (check-list-clear!)
  (check-list (make-queue)))

(define-macro (define-enum name . syms)
  (do ((i 0 (+ i 1))
       (rest syms (cdr rest))
       (exprs '() (cons `(define-constant ,(car rest) ,i) exprs)))
      ((null? rest) `(begin ,@exprs))))

(define-enum .intermediate-tags.
  $DEFINE
  $LREF
  $LSET
  $GREF
  $GSET
  $CONST
  $IF
  $LET
  $RECEIVE
  $LAMBDA
  $LABEL
  $PROMISE
  $SEQ
  $CALL
  $ASM
  $CONS
  $APPEND
  $VECTOR
  $LIST->VECTOR
  $LIST
  $LIST*
  $MEMV
  $EQ?
  $EQV?
  $IT
  )

(define code->mnemonic
  (let ((tbl (apply hash-table 'equal?
                    (map (match-lambda
                          ((k . v)
                           (cons (slot-ref v 'code) k)))
                         (class-slot-ref <vm-insn-info> 'all-insns)))))
    (lambda (code)
      (hash-table-get tbl code #f))))

(define asm-call
  (let ((insn-proc-alist `((APPEND . ,append)
                           (APPLY . ,apply)
                           (ASSQ . ,assq)
                           (ASSV . ,assv)
                           (CAAR . ,caar)
                           (CADR . ,cadr)
                           (CAR . ,car)
                           (CDAR . ,cdar)
                           (CDDR . ,cddr)
                           (CDR . ,cdr)
                           (CHARP . ,char?)
                           (CONS . ,cons)
                           (CONSTU . ,undefined)
                           (CURERR . ,current-error-port)
                           (CURIN . ,current-input-port)
                           (CUROUT . ,current-output-port)
                           (EOFP . ,eof-object?)
                           (EQ . ,eq?)
                           (EQV . ,eqv?)
                           (IDENTIFIERP . ,identifier?)
                           (IS-A . ,is-a?)
                           (LENGTH . ,length)
                           (LIST . ,list)
                           (LIST-STAR n . ,list*)
                           (MEMQ . ,memq)
                           (MEMV . ,memv)
                           (NEGATE . ,-)
                           (NOT . ,not)
                           (NULLP . ,null?)
                           (NUMADD2 . ,+)
                           (NUMDIV2 . ,/)
                           (NUMEQ2 . ,=)
                           (NUMGE2 . ,>=)
                           (NUMGT2 . ,>)
                           (NUMIADD2 . ,+.)
                           (NUMIDIV2 . ,/.)
                           (NUMIMUL2 . ,*.)
                           (NUMISUB2 . ,-.)
                           (NUMLE2 . ,<=)
                           (NUMLT2 . ,<)
                           (NUMMUL2 . ,*)
                           (NUMSUB2 . ,-)
                           (PAIRP . ,pair?)
                           (PEEK-CHAR . ,peek-char)
                           (READ-CHAR . ,read-char)
                           (REVERSE . ,reverse)
                           (SETTER . ,setter)
                           (SLOT-REF . ,slot-ref)
                           (SLOT-SET . ,slot-set!)
                           (STRINGP . ,string?)
                           (SYMBOLP . ,symbol?)
                           (VALUES . ,values)
                           (VEC . ,vector)
                           (VEC-REF . ,vector-ref)
                           (VEC-SET . ,vector-set!)
                           (VECTORP . ,vector?)
                           (WRITE-CHAR . ,write-char))))
    (lambda (env insn args)
      (cond
       ((assoc (code->mnemonic insn) insn-proc-alist)
        => (lambda (pair)
             (delay-call env (value-object (cdr pair)) args)))
       (else
        (value-unknown))))))

;; IForm scanner
(define-macro (case-iform iform . pat-list)
  (let ((iform-type (gensym))
        (src (gensym)))
    `(let ((,iform-type (vector-ref ,iform 0)))
       ,(let loop ((rest pat-list))
          (cond
           ((null? rest) #f)
           (else
            `(if (eq? ,iform-type ,(caar (list-ref rest 0)))
                 (let ,(do ((i 1 (+ i 1))
                            (args (cdar (list-ref rest 0)) (cdr args))
                            (lvars '()))
                           ((null? args)
                            lvars)
                         (case (car args)
                           (($src)
                            (push! lvars `(,src (vector-ref ,iform ,i))))
                           ((_))
                           (else
                            (push! lvars `(,(car args) (vector-ref ,iform ,i))))))
                   ,(if (memq '$src (cdar (list-ref rest 0)))
                        `(with-src ,src ,@(cdr (list-ref rest 0)))
                        `(begin ,@(cdr (list-ref rest 0)))))
                 ,(loop (cdr rest)))))))))

(define (make-multi-value-length-checker dval lvar-len)
  (let ((sinf (src-info)))
    (lambda ()
      (with-src-info sinf
                     (let* ((val (force dval))
                            (mv-len (multi-value-length val)))
                       (when (and (not (is-a? val <value-unknown>))
                                  (and mv-len (not (= lvar-len mv-len))))
                         (print-errmsg "expected ~a values, but got ~a values"
                                       lvar-len
                                       mv-len)))))))

(define (monotonic-merge-cpl start cpls)
  (define (%monotonic-merge lst cpls)
    (cond
     ((null? cpls)
      (reverse (cons (value-object <top>) lst)))
     (else
      (let ((c (caar cpls)))
        (cond
         ((or (equal? (value-object <top>) c)
              (member c lst))
          (let ((rest-cpl (cdar cpls)))
            (%monotonic-merge lst (if (null? rest-cpl)
                                      (cdr cpls)
                                      (append (cdr cpls) (list rest-cpl))))))
         ((any (lambda (cpl)
                 (member c (cdr cpl)))
               (cdr cpls))
          (%monotonic-merge lst (append (cdr cpls) (list (car cpls)))))
         (else
          (let ((rest-cpl (cdar cpls)))
            (%monotonic-merge (cons c lst)
                              (if (null? rest-cpl)
                                  (cdr cpls)
                                  (append (cdr cpls) (list rest-cpl)))))))))))
  (%monotonic-merge (list start) cpls))
            
(define (make-cpl env vclass direct-super-symbols)
  (monotonic-merge-cpl vclass (filter values
                                      (map (lambda (sym)
                                             (let ((k (force (delay-gref-value env sym))))
                                               (if (equal? k (value-unknown))
                                                   #f
                                                   (value-class-precedence-list k))))
                                           direct-super-symbols))))

(define (delay-call env proc args)
  (let* ((sinf (src-info))
         (prms (delay (with-src-info
                       sinf
                       (call env (force proc) (map force args))))))
    (check-list-add! (lambda () (force prms)))
    prms))

(define (delay-gref-value env id)
  (let ((v (let ((sinf (src-info))
                 (mod (id-module id))
                 (sym (id-symbol id)))
             (delay (with-src-info
                     sinf
                     (cond
                      ((env-global-ref env mod sym)
                       => (cut force <>))
                      (else
                       (print-errmsg "~a(~a) referenced but not defined" sym mod)
                       (value-unknown))))))))
    (check-list-add! (lambda ()
                       (force v)))
    v))

(define (delay-lref-value env id)
  (let ((sinf (src-info)))
    (delay (with-src-info
            sinf
            (cond
             ((env-local-ref env id)
              => (cut force <>))
             (else
              (print-errmsg "~a referenced but not defined" (id-symbol id))
              (value-unknown)))))))

(define (delay-one-value val)
  (let ((ov (delay (one-value (force val)))))
    (check-list-add! (lambda ()
                       (force ov)))
    ov))

(define (make-value-class env module name supers slot-specs metaclass)
  (let ((vc (if (value-class? metaclass)
                (make <value-class>
                  :module module
                  :name name
                  :direct-supers supers
                  :slot-specs slot-specs
                  :metaclass metaclass)
                (begin
                  (print-errmsg "<class> required, but got ~a" (x->string metaclass))
                  (value-unknown)))))
    (when (is-a? vc <value-class>)
      (set! (cpl-of vc) (make-cpl env vc supers))
      (for-each (lambda (slot-spec)
                  (cond
                   ((get-keyword :accessor (cdr slot-spec) #f)
                    => (lambda (proc-name)
                         (env-define-method env (module-name module) proc-name
                                            (list vc) #f
                                            (value-lambda proc-name 1 0
                                                          (lambda _ (value-unknown))))))
                   ((get-keyword :getter (cdr slot-spec) #f)
                    => (lambda (proc-name)
                         (env-define-method env (module-name module) proc-name
                                            (list vc) #f
                                            (value-lambda proc-name 1 0
                                                          (lambda _ (value-unknown))))))
                   ((get-keyword :setter (cdr slot-spec) #f)
                    => (lambda (proc-name)
                         (env-define-method env (module-name module) proc-name
                                            (list vc (value-object <top>))
                                            #f
                                            (value-lambda proc-name 2 0
                                                          (lambda _ (value-unknown))))))
                   (else
                    #f)))
                slot-specs))
    vc))

;; scan-iform returns <promise> or value-object
(define (scan-iform env iform)
  (case-iform
   iform

   (($DEFINE $src _ id body)
    (env-global-define env id (one-value (force (scan-iform env body))))
    (guard (e (else #f))
      (eval (vector-ref iform 1) (vm-current-module)))
    (value-object (id-symbol id)))

   (($LREF lvar)
    (delay-lref-value env (lvar-id lvar)))

   (($LSET lvar iform)
    ;; ローカル変数が書き換えられる場合は先に unknown を設定しているので、ここで更新は行わない
    (scan-iform env iform))

   (($GREF id)
    (delay-gref-value env id))

   (($GSET id iform)
    ;; グローバル変数が書き換えられる場合は一律 unknown として扱う
    (let ((v (scan-iform env iform)))
      (env-global-define env id (value-unknown))
      v))

   (($CONST obj)
    (value-object obj))

   (($IF $src expr-iform then-iform else-iform)
    ;; $it のために新たな環境を生成している
    (let* ((local-env (make-env env))
           (expr-dval (scan-iform local-env expr-iform)))
      (env-set-it local-env expr-dval)
      ;; global reference で更新されるものを検出するため、いったんthen-iform, else-iform
      ;; を調べる。ただし、それ以外の結果(関数呼び出しや変数の参照などのチェック)はすべて破棄する
      (parameterize ((check-list (make-queue)))
        (let ((local-env (make-env env)))
          (scan-iform local-env then-iform)
          (scan-iform local-env else-iform)))
      (delay (let ((expr-val (force expr-dval)))
               expr-iform
               (cond
                ((and (is-a? expr-val <value-object>)
                      (object-of expr-val))
                 (force (scan-iform local-env then-iform)))
                ((and (is-a? expr-val <value-object>)
                      (not (object-of expr-val)))
                 (force (scan-iform local-env else-iform)))
                (else
                 (combine-value (force (scan-iform local-env then-iform))
                                (force (scan-iform local-env else-iform)))))))))

   (($LET $src type lvar-list lvar-iform-list body-iform)
    (match (iform->sexp iform)
      (('$LET ((_ ('$CALL ('$GREF _ 'make)
                          ('$CALL ('$GREF _ '%get-default-metaclass)
                                  ('$CALL ('$GREF _ 'list) ('$GREF _ _) ...))
                          ('$CONST :name) ('$CONST name)
                          ('$CONST :supers) ('$CALL ('$GREF _ 'list) ('$GREF supers _)
                                                    ...)
                          ('$CONST :slots) ('$CALL ('$GREF _ 'list)
                                                   ('$CALL ('$GREF _ 'list)
                                                           (or ('$CONST slots-specs)
                                                               slots-specs) ...)
                                                   ...)
                          ('$CONST :defined-modules) ('$CALL ('$GREF _ 'list)
                                                             ('$CONST module)))))
              ('$SEQ _ ...))
       (make-value-class env module name supers slots-specs (value-object <class>)))
      (('$LET ((_ ('$CALL ('$GREF _ 'make)
                          ('$GREF _ _)
                          ('$CONST :name) ('$CONST name)
                          ('$CONST :supers) ('$CALL ('$GREF _ 'list) ('$GREF supers _)
                                                    ...)
                          ('$CONST :slots) ('$CALL ('$GREF _ 'list)
                                                   ('$CALL ('$GREF _ 'list)
                                                           (or ('$CONST slots-specs)
                                                               slots-specs) ...)
                                                   ...)
                          ('$CONST :defined-modules) ('$CALL ('$GREF _ 'list)
                                                             ('$CONST module))
                          ('$CONST :metaclass) ('$GREF metaclass _))))
              ('$SEQ _ ...))
       (make-value-class env module name supers slots-specs (force (delay-gref-value env metaclass))))
      (`($LET ((,_ ($CALL ($GREF ,_ %ensure-generic-function)
                          ($CONST ,method-name)
                          ($CONST ,module))))
              ($SEQ ($CALL ($GREF ,_ add-method!)
                           ($LREF ,_)
                           ($CALL ($GREF ,_ make)
                                  ($GREF ,_ <method>)
                                  ($CONST :generic) ($LREF ,_)
                                  ($CONST :specializers) ($CALL ($GREF ,_ list)
                                                                ($GREF ,arg-types ,_)
                                                                ...)
                                  ($CONST :lambda-list) ($CONST ,arg-vals)
                                  ($CONST :body) ,body-iform))
                    ($LREF ,_)))
       (env-define-method env (module-name module) method-name
                          (map (lambda (id)
                                 (force (delay-gref-value env id)))
                               arg-types)
                          (not (proper-list? arg-vals))
                          (force (scan-iform env body-iform)))
       (value-unknown))
      (else
       (let ((local-env (make-env env)))
         (for-each (lambda (lvar iform)
                     (let ((v (scan-iform (if (eq? type 'let)
                                              env
                                              local-env)
                                          iform)))
                       (env-local-define local-env lvar (delay-one-value v))))
                   lvar-list
                   lvar-iform-list)
         (scan-iform local-env body-iform)))))

   (($RECEIVE $src reqarg optarg lvar-list expr-iform body-iform)
    (let ((local-env (make-env env))
          (dv (scan-iform env expr-iform)))
      (do ((i 0 (+ i 1))
           (lvar-rest lvar-list (cdr lvar-rest)))
          ((<= reqarg i)
           (cond
            ((not (= optarg 0))
             (env-local-define local-env
                               (car lvar-rest)
                               (delay (multi-value-rest (force dv) i))))
            (else
             (check-list-add! (make-multi-value-length-checker dv
                                                               (length lvar-list))))))
        (let ((lv (delay (multi-value-ref (force dv) i))))
          (env-local-define local-env (car lvar-rest) lv)))
      (scan-iform local-env body-iform)))

   (($LAMBDA $src name reqarg optarg lvar-list body-iform flag)
    ;; global reference で更新されるものを検出するため、いったんbody-iformを調べる
    ;; ただし、それ以外の結果(関数呼び出しや変数の参照などのチェック)はすべて破棄する
    (parameterize ((check-list (make-queue)))
      (let ((local-env (make-env env)))
        (scan-iform local-env body-iform)))
    (let ((v (value-lambda name reqarg optarg 
                           (let ((counter 0))
                             (lambda val-list
                               (let ((local-env (make-env env)))
                                 (cond
                                  ((<= 2 counter)
                                   ;; lambdaが2回以上再帰呼び出しされた場合は
                                   ;; 無条件に unknown を返す
                                   (value-unknown))
                                  (else
                                   (inc! counter)
                                   (do ((lval-rest lvar-list (cdr lval-rest))
                                        (val-rest val-list (cdr val-rest))
                                        (i 0 (+ i 1)))
                                       ((= reqarg i)
                                        (unless (= optarg 0)
                                          (env-local-define local-env
                                                            (car lval-rest)
                                                            (value-instance (value-object <list>)))))
                                     (env-local-define local-env
                                                       (car lval-rest)
                                                       (car val-rest)))
                                   (begin0
                                     (force (scan-iform local-env body-iform))
                                     (dec! counter 0))))))))))
      (check-list-add! (lambda ()
                         (call env v (make-list (+ reqarg optarg) (value-unknown)))))
      v))
   
   ;; The pass2 optimization generates $label,
   ;; so this check will not be executed.
   (($LABEL $src label iform)
    (scan-iform env iform))

   (($PROMISE $src expr-iform)
    (let ((sinf (src-info)))
      (value-promise (delay (with-src-info
                             sinf
                             (call env (force (scan-iform env expr-iform)) '()))))))

   (($SEQ iform-list)
    (fold (lambda (iform val)
            (let ((v (scan-iform env iform)))
              (check-list-add! (lambda ()
                                 (force v)))
              v))
          #f
          iform-list))
   
   (($CALL $src proc-iform arg-iform-list flag)
    (let* ((proc (scan-iform env proc-iform))
           (args (map (lambda (iform)
                        (scan-iform env iform))
                      arg-iform-list)))
      (delay-call env proc args)))

   (($ASM $src insn arg-iform-list)
    (let ((args (map (cut scan-iform env <>) arg-iform-list)))
      (asm-call env (car insn) args)))

   (($CONS $src ca-iform cd-iform)
    (delay-call env (value-object cons)
                (map (cut scan-iform env <>) (list ca-iform cd-iform))))

   (($APPEND $src ca-iform cd-iform)
    (delay-call env (value-object append)
                (map (cut scan-iform env <>) (list ca-iform cd-iform))))

   (($VECTOR $src elt-list)
    (delay-call env (value-object vector)
                (map (cut scan-iform env <>) elt-list)))

   (($LIST->VECTOR $src list-iform)
    (delay-call env (value-object list->vector)
                (list (scan-iform env list-iform))))

   (($LIST $src elt-list)
    (delay-call env (value-object list)
                (map (cut scan-iform env <>) elt-list)))

   (($LIST* $src elt-list)
    (delay-call env (value-object list*)
                (map (cut scan-iform env <>) elt-list)))

   (($MEMV $src obj-iform list-iform)
    (delay-call env (value-object memv)
                (map (cut scan-iform env <>) (list obj-iform list-iform))))

   (($EQ? $src x-iform y-iform)
    (delay-call env (value-object eq?)
                (map (cut scan-iform env <>) (list x-iform y-iform))))

   (($EQV? $src x-iform y-iform)
    (delay-call env (value-object eqv?)
                (map (cut scan-iform env <>) (list x-iform y-iform))))

   (($IT)
    (env-it env))))

(define-macro (case/unquote obj . clauses)
  (let1 tmp (gensym)
    (define (expand-clause clause)
      (match clause
        (((item) . body)
         `((eqv? ,tmp ,item) ,@body))
        (((item ...) . body)
         (let1 ilist (list 'quasiquote
                           (map (cut list 'unquote <>) item))
           `((memv ,tmp ,ilist) ,@body)))
        (('else . body)
         `(else ,@body))))
    `(let ((,tmp ,obj))
       (cond ,@(map expand-clause clauses)))))

(define (iform->sexp iform)
  (define (lvar-id lvar)
    (vector-ref lvar 1))

  (define (rec iform)
    (case/unquote
     (vector-ref iform 0)
     (($DEFINE)
      `($DEFINE ,(id-symbol (vector-ref iform 3)) ,(rec (vector-ref iform 4))))
     (($LREF)
      `($LREF ,(lvar-id (vector-ref iform 1))))
     (($LSET)
      `($LSET ,(lvar-id (vector-ref iform 1)) ,(rec (vector-ref iform 2))))
     (($GREF)
      `($GREF ,(vector-ref iform 1) ,(id-symbol (vector-ref iform 1))))
     (($GSET)
      `($GSET ,(id-symbol (vector-ref iform 1)) ,(rec (vector-ref iform 2))))
     (($CONST)
      `($CONST ,(vector-ref iform 1)))
     (($IF)
      `($IF ,(rec (vector-ref iform 2))
            ,(rec (vector-ref iform 3))
            ,(rec (vector-ref iform 4))))
     (($LET)
      `($LET ,(map (lambda (lvar lvar-iform)
                     `(,(lvar-id lvar) ,(rec lvar-iform)))
                   (vector-ref iform 3)
                   (vector-ref iform 4))
             ,(rec (vector-ref iform 5))))
     (($RECEIVE)
      `($RECEIVE ,(map lvar-id (vector-ref iform 4)) ,(rec (vector-ref iform 5))
                 ,(rec (vector-ref iform 6))))
     (($LAMBDA)
      iform)
     (($LABEL)
      `($LABEL ,(vector-ref iform 2) ,(rec (vector-ref iform 3))))
     (($PROMISE)
      `($PROMISE ,(rec (vector-ref iform 2))))
     (($SEQ)
      `($SEQ ,@(map rec (vector-ref iform 1))))
     (($CALL)
      `($CALL ,(rec (vector-ref iform 2)) ,@(map rec (vector-ref iform 3))))
     (($ASM)
      `($ASM ,(code->mnemonic (car (vector-ref iform 2)))
             ,@(map rec (vector-ref iform 3))))
     (($IT)
      '($IT))
     (($CONS)
      `($CONS ,(rec (vector-ref iform 2)) ,(rec (vector-ref iform 3))))
     (($APPEND)
      `($APPEND ,(rec (vector-ref iform 2)) ,(rec (vector-ref iform 3))))
     (($VECTOR)
      `($VECTOR ,@(map rec (vector-ref iform 2))))
     (($LIST->VECTOR)
      `($LIST->VECTOR ,(rec (vector-ref iform 2))))
     (($LIST)
      `($LIST ,@(map rec (vector-ref iform 2))))
     (($LIST*)
      `($LIST* ,@(map rec (vector-ref iform 2))))
     (($MEMV)
      `($MEMV ,(rec (vector-ref iform 2)) ,(rec (vector-ref iform 3))))
     (($EQ?)
      `($EQ? ,(rec (vector-ref iform 2)) ,(rec (vector-ref iform 3))))
     (($EQV?)
      `($EQV? ,(rec (vector-ref iform 2)) ,(rec (vector-ref iform 3))))
     (else
      '())))
  (rec iform))

(define (make-cenv)
  (let ((cenv (make-vector 5 #f)))
    (vector-set! cenv 0 (vm-current-module))
    (vector-set! cenv 1 '())
    cenv))

(define (scan-expr env expr . rest)
  (let-optionals* rest ((filename '-)
                        (line-no '-))
    (let ((cenv (make-cenv)))
      (with-src-info
       (or (and (pair? expr)
                (debug-source-info expr))
           (make-src-info filename line-no))
       (let ((iform (guard (e (else (print-errmsg (slot-ref e 'message))
                                    #f))
                      (pass1 expr cenv))))
         (if iform
             ;; トップレベルの扱いなので、ここで値を確定させてしまう
             (force (scan-iform env iform))
             (value-unknown)))))))

(define (pp-iform expr)
  (let ((iform (pass1 expr (make-cenv))))
    (do ((i 0 (+ i 1)))
        ((<= (vector-length iform) i))
      (format #t "iform[~a] = " i)
      (write/ss (vector-ref iform i))
      (newline))
    (newline)
    ((with-module gauche.internal pp-iform) iform)))

(define (scan-port env in)
  (define (modify-errmsg msg)
    (cond
     ((#/Read error at .+:line \d+: (.*)/ msg)
      => (lambda (m)
           (m 1)))
     (else
      msg)))
  (define (skip-whitespace in)
    (let loop ((c (peek-char in)))
      (cond
       ((eof-object? c)
        #f)
       ((char-set-contains? char-set:whitespace c)
        (read-char in)
        (loop (peek-char in)))
       (else
        #t))))
  (when (skip-whitespace in)
    (let loop ((line-no (port-current-line in)))
      (let ((expr (guard (e (else (with-src-info
                                   (make-src-info (port-name in) line-no)
                                   (print-errmsg (modify-errmsg
                                                  (slot-ref e 'message))))
                                  #f))
                    (read in))))
        (cond
         ((eof-object? expr)
          #t)
         (else
          ;;
          (scan-expr env expr (port-name in) line-no)
          (when (skip-whitespace in)
            (loop (port-current-line in)))))))))

(define (scan-file env filename)
  (call-with-input-file filename
    (lambda (in)
      (scan-port env in))))

(define (main args)
  (define (uniq-msg msg-list)
    (map first (group-sequence (sort msg-list (lambda (elt0 elt1)
                                                (let ((filename0 (first (second elt0)))
                                                      (line0 (second (second elt0)))
                                                      (msg0 (third elt0))
                                                      (filename1 (first (second elt1)))
                                                      (line1 (second (second elt1)))
                                                      (msg1 (third elt1)))
                                                  (cond
                                                   ((and (equal? filename0 filename1)
                                                         (equal? line0 line1))
                                                    (string<? msg0 msg1))
                                                   ((equal? filename0 filename1)
                                                    (< line0 line1))
                                                   (else
                                                    (string<? filename0 filename1))))))
                               :test equal?)))
  (let ((env (make-initial-env)))
    (with-pool-error-message (lambda ()
                               (cond
                                ((null? args)
                                 (scan-port env (current-input-port)))
                                (else
                                 (for-each (cut scan-file env <>) args)))
                               (do-check))
                             (lambda (result msg-list)
                               (for-each (cut apply print-msg <>)
                                         (uniq-msg msg-list)))))
  0)

;; Epilogue
(provide "codecheck")


