;; -*- coding: utf-8; mode: scheme -*-
;;
;; value.scm 
;;
;;   Copyright (c) 2008 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
(define-module codecheck.value
  (use srfi-1)
  (use util.match)
  (use codecheck.util)

  (export-all)
  )

(select-module codecheck.value)

(define-class <procedure-type> ()
  ((procecure :init-value #f
              :accessor procedure-of
              :init-keyword :procedure)
   (arg-type-tree :init-form (list #f)
                  :accessor arg-type-tree-of)
   (methods :init-value '()
            :accessor methods-of)))

;; class definition
(define-class <value> ()
  ())

(define-class <value-unknown> (<value>)
  ())

(define-class <value-object> (<value>)
  ((object :init-keyword :object)
   (undefined? :init-value #f
               :init-keyword :undefined?)))

(define-class <value-class> (<value>)
  ((module :init-keyword :module)
   (name :init-keyword :name)
   (cpl :init-keyword :cpl
        :accessor cpl-of)
   (direct-supers :init-keyword :direct-supers)
   (slot-specs :init-keyword :slot-specs)
   (metaclass :init-keyword :metaclass
              :accessor value-metaclass-of)))

(define-class <value-instance> (<value>)
  ((class :init-keyword :class
          :accessor value-class-of)))

(define-class <value-lambda> (<value>)
  ((name :init-keyword :name
         :accessor name-of)
   (reqarg :init-keyword :reqarg
           :accessor reqarg-of)
   (optarg :init-keyword :optarg
           :accessor optarg-of)
   (proc :init-keyword :proc
         :accessor value-maker-of)))

(define-class <value-promise> (<value>)
  ((promise :init-keyword :promise)))

(define-class <value-generic> (<value>)
  ((name :init-keyword :name
         :accessor name-of)
   (procedure-type :init-keyword :procedure-type
                   :accessor procedure-type-of)))

(define-class <value-union> (<value>)
  ((values :init-keyword :values
           :accessor values-of)))

(define-class <multi-value> ()
  ((values :init-keyword :values)))


;; x->string
(define-method x->string ((v <value-unknown>))
  "#<unidentified>")

(define-method x->string ((v <value-object>))
  (format "~s" (object-of v)))

(define-method x->string ((v <value-class>))
  (format "#<class ~a>" (slot-ref v 'name)))

(define-method x->string ((v <value-instance>))
  (format "#<an instance of ~a>" (value-class-name (slot-ref v 'class))))

(define-method x->string ((v <value-lambda>))
  (if (name-of v)
      (format "#<closure ~a>" (name-of v))
      (format "#<closure #f>")))

(define-method x->string ((v <value-promise>))
  "#<promise>")

(define-method x->string ((v <value-generic>))
  (format "#<generic ~a>" (name-of v)))

(define-method x->string ((v <value-union>))
  (format "#<one of these values (~a)>"
          (string-join (map x->string (slot-ref v 'values)) ", ")))

(define-method x->string ((v <multi-value>))
  (format "#<~a values (~a)>"
          (length (slot-ref v 'values))
          (string-join (map x->string (slot-ref v 'values)) ", ")))

;; object-equal?
(define-method object-equal? ((v1 <value-unknown>) (v2 <value-unknown>))
  #t)

(define-method object-equal? ((v1 <value-object>) (v2 <value-object>))
  (equal? (object-of v1) (object-of v2)))

(define-method object-equal? ((v1 <value-class>) (v2 <value-class>))
  (boolean (every (lambda (x)
                    (equal? (slot-ref v1 x) (slot-ref v2 x)))
                  '(module name cpl direct-supers slot-specs metaclass))))

(define-method object-equal? ((v1 <value-instance>) (v2 <value-instance>))
  (equal? (slot-ref v1 'class) (slot-ref v2 'class)))

(define-method object-equal? ((v1 <value-lambda>) (v2 <value-lambda>))
  (boolean (and (every (lambda (x)
                         (equal? (slot-ref v1 x) (slot-ref v2 x)))
                       '(name reqarg optarg))
                (eq? (slot-ref v1 'proc) (slot-ref v2 'proc)))))

(define-method object-equal? ((v1 <value-promise>) (v2 <value-promise>))
  (eq? (slot-ref v1 'promise) (slot-ref v2 'promise)))

(define-method object-equal? ((v1 <value-generic>) (v2 <value-generic>))
  (eq? v1 v2))

(define-method object-equal? ((v1 <value-union>) (v2 <value-union>))
  (equal? (slot-ref v1 'values) (slot-ref v2 'values)))

(define-method object-equal? ((v1 <multi-value>) (v2 <multi-value>))
  (equal? (slot-ref v1 'values) (slot-ref v2 'values)))


;; constructors 
(define (value-unknown)
  (make <value-unknown>))

(define (value-object obj)
  (if (undefined? obj)
      (make <value-object> :object #f :undefined? #t)
      (make <value-object> :object obj :undefined? #f)))

(define-method object-of ((v <value-object>))
  (if (slot-ref v 'undefined?)
      (undefined)
      (slot-ref v 'object)))

(define-method value-instance ((vclass <value-class>))
  (make <value-instance> :class vclass))

(define-method value-instance ((klass <class>))
  (make <value-instance> :class (value-object klass)))

(define-method value-instance ((v <value-object>))
  (if (is-a? (object-of v) <class>)
      (value-instance (object-of v))
      (begin
        (print-errmsg "<class> required, but got ~a" (x->string v))
        (value-unknown))))

(define-method value-instance ((v <value-unknown>))
  (value-unknown))

(define (value-lambda name reqarg optarg proc)
  (make <value-lambda> :name name :reqarg reqarg :optarg optarg :proc proc))

(define-method value-class? ((v <value>))
  #f)

(define-method value-class? ((v <value-object>))
  (is-a? (object-of v) <class>))

(define-method value-class? ((v <value-instance>))
  (boolean (member <class> (value-class-precedence-list (value-class-of v)))))

(define-method value-class? ((v <value-class>))
  #t)

(define-method value-class? ((vu <value-union>))
  (find values (map value-class? (values-of vu))))

(define-method value-class? ((mv <multi-value>))
  (value-class? (one-value mv)))
           
(define (value-promise prms)
  (make <value-promise> :promise prms))

(define (value-generic name)
  (make <value-generic> :name name :procedure-type (make <procedure-type> :name #f)))

(define (value-union vals)
  (make <value-union> :values vals))



(define-method value-class-precedence-list ((val <value>))
  (print-errmsg "<class> required, but got ~a" (x->string val))
  '())

(define-method value-class-precedence-list ((val <value-object>))
  (let ((klass (object-of val)))
    (if (is-a? klass <class>)
        (map value-object (class-precedence-list klass))
        (next-method))))

(define-method value-class-precedence-list ((val <value-class>))
  (slot-ref val 'cpl))

(define-method value-class-name ((val <value>))
  (print-errmsg "<class> required, but got ~a" (x->string val))
  "???")

(define-method value-class-name ((val <value-object>))
  (let ((klass (object-of val)))
    (if (is-a? klass <class>)
        (class-name klass)
        (next-method))))

(define-method value-class-name ((val <value-class>))
  (slot-ref val 'name))

(define-method force-value ((v <value>))
  v)

(define-method force-value ((v <value-promise>))
  (force (slot-ref v 'promise)))

(define-method force-value ((mv <multi-value>))
  (force-value (one-value mv)))

(define (multi-value vals)
  (make <multi-value> :values vals))

(define-method multi-value-ref ((val <value>) (i <integer>))
  (if (= i 0)
      val
      (begin
        (print-errmsg "reference of values index out of range: ~a (got 1 value)" i)
        (value-unknown))))

(define-method multi-value-ref ((val <multi-value>) (i <integer>))
  (if (< i (multi-value-length val))
      (list-ref (slot-ref val 'values) i)
      (begin
        (print-errmsg "reference of values index out of range: ~a (got ~a values)"
                      i
                      (multi-value-length val))
        (value-unknown))))

(define-method multi-value-ref ((val <value-unknown>) (i <integer>))
  (value-unknown))


(define-method multi-value-rest ((val <value-unknown>) (i <integer>))
  (value-instance <list>))

(define-method multi-value-rest ((mv <multi-value>) (i <integer>))
  (let ((vals (slot-ref mv 'values)))
    (if (< i (length vals))
        (value-instance (value-object <list>))
        (begin
          (print-errmsg "values-ref index out of range: ~a (got ~a values)"
                        i
                        (length vals))
          (value-unknown)))))

(define-method multi-value-rest ((val <value>) (i <integer>))
  (if (= i 0)
      (value-instance <list>)
      (begin
        (print-errmsg "values-ref index out of range: ~a (got 1 value)" i)
        (value-unknown))))


(define-method multi-value-length ((val <value>))
  1)

(define-method multi-value-length ((mv <multi-value>))
  (length (slot-ref mv 'values)))

(define-method one-value ((v <value>))
  v)

(define-method one-value ((mv <multi-value>))
  (let ((len (multi-value-length mv)))
    (unless (= len 1)
      (print-warnmsg "the continuation expects one value, but got ~a values" len))
    (multi-value-ref mv 0)))


(define-method combine-value ((v <value>))
  v)

(define-method combine-value ((mv <multi-value>))
  mv)

(define-method combine-value ((v1 <value>) (v2 <value>))
  (if (equal? v1 v2)
      v1
      ;; 本来は<value-union>を返すべきだが、述語の実装をきちんと行わないと誤判定する
      ;; ケース(e.g. binary/pack.scm の1050行目のlet-keywords)があるため、
      ;; 今のところは<value-unknown>を返す
      (value-unknown)))

(define-method combine-value ((v1 <value-unknown>) v2)
  (value-unknown))

(define-method combine-value (v1 (v2 <value-unknown>))
  (value-unknown))

(define-method combine-value ((mv1 <multi-value>) (mv2 <multi-value>))
  (if (= (multi-value-length mv1) (multi-value-length mv2))
      (multi-value (map combine-value
                        (slot-ref mv1 'values)
                        (slot-ref mv2 'values)))
      (value-unknown)))

(define-method combine-value ((mv1 <multi-value>) (v2 <value>))
  (if (= (multi-value-length mv1) 1)
      (combine-value (one-value mv1) v2)
      (value-unknown)))

(define-method combine-value ((v1 <value>) (mv2 <multi-value>))
  (combine-value mv2 v1))

(define-method combine-value ((v1 <value-union>) (v2 <value>))
  (value-union (cons v2 (slot-ref v1 'values))))

(define-method combine-value ((v1 <value>) (v2 <value-union>))
  (combine-value v2 v1))

(define-method combine-value ((v1 <value-union>) (v2 <value-union>))
  (value-union (append (slot-ref v1 'values) (slot-ref v2 'values))))

(define-method combine-value (v1 v2 . rest)
  (apply combine-value (combine-value v1 v2) rest))

;; misc.
(define (value-number)
  (value-union (map value-instance (list <integer> <rational> <real> <complex> <number>))))


(provide "codecheck/value")

;; end of file
