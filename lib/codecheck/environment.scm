;; -*- coding: utf-8; mode: scheme -*-
;;
;; environment.scm - 
;;
;;   Copyright (c) 2008 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define-module codecheck.environment
  (use srfi-1)
  (use gauche.parameter)
  (use gauche.sequence)
  (use util.match)
  (use util.queue)

  (use codecheck.util)
  (use codecheck.value)
  (use util.list)
  (use srfi-13)
  (use file.util)

  (export-all)
  )

(select-module codecheck.environment)

;; environment
(define-class <environment> ()
  ((parent :init-value #f
           :accessor parent-of
           :init-keyword :parent)
   (module-table :init-value #f
                 :accessor module-table-of
                 :allocation :class)
   (local-alist :init-form (list)
                :accessor local-alist-of)
   (it :init-form (value-unknown))))

(define (make-env parent)
  (make <environment> :parent parent))

(define (make-initial-env)
  (let ((env (make-env #f)))
    (set! (module-table-of env) (make-hash-table 'eq?))
    env))

(define (env-global-table env module-name)
  (let ((module-table (module-table-of env)))
    (or (hash-table-get module-table module-name #f)
        (begin
          (let ((tbl (make-hash-table 'eq?)))
            (hash-table-put! module-table module-name tbl)
            tbl)))))

(define (env-global-ref env module-name sym)
  (or (let ((global-table (env-global-table env module-name)))
        (hash-table-get global-table sym #f))
      (let ((m (find-module module-name)))
        (if m
            (any (lambda (mod)
                   (cond
                    ((global-variable-ref mod sym #f)
                     => (cut value-object <>))
                    (else
                     #f)))
                 (append (list m) (module-imports m) (module-precedence-list m)))
            #f))))

(define (env-global-set! env module-name sym val)
  (let ((global-table (env-global-table env module-name)))
    (hash-table-put! global-table sym val)))

(define (env-local-ref env id)
  (cond
   ((assq-ref (local-alist-of env) id #f) => (cut values <>))
   ((parent-of env) => (cut env-local-ref <> id))
   (else #f)))

(define (env-local-set! env id val)
  (set! (local-alist-of env) (assq-set! (local-alist-of env) id val)))

(define (env-global-define env id val)
  (env-global-set! env (id-module id) (id-symbol id) val))
    

(define (env-local-define env lvar val)
  (env-local-set! env (lvar-id lvar) (if (lvar-modified? lvar)
                                        (value-unknown)
                                        val)))

(define (env-define-method env mod sym specializers optional? body-lambda)
  (let* ((obj (env-global-ref env mod sym))
         (proc-type (cond
                     ((or (not obj)
                          (and (is-a? obj <value-instance>)
                               (value-class-match? env obj (value-object <generic>))))
                      (let* ((vg (value-generic sym)))
                        (env-global-set! env mod sym vg)
                        (procedure-type-of vg)))
                     ((and (is-a? obj <value-object>)
                           (procedure? (object-of obj)))
                      (if (hash-table-exists? proc-table (object-of obj))
                          (hash-table-get proc-table (object-of obj))
                          (let ((pt (make <procedure-type>
                                      :procedure (object-of obj))))
                            (sync-procedure! pt)
                            (hash-table-put! proc-table (object-of obj) pt)
                            pt)))
                     ((and (is-a? obj <value-generic>))
                      (procedure-type-of obj)))))
    (add-specializer! proc-type
                      (lambda (env args)
                        (apply (value-maker-of body-lambda)
                               (if optional?
                                   (append (take args (length specializers))
                                           (list (value-instance <list>)
                                                 (value-unknown)))
                                   (append args (list (value-unknown))))))
                      (append specializers (if optional? '(|...|) '())))))
                         
(define (env-set-it env val)
  (set! (ref env 'it) val))

(define (env-it env)
  (ref env 'it))

(define (arity-result->string result)
  (cond
   ((integer? result)
    (format "~a" result))
   ((arity-at-least? result)
    (format "more than ~a" (arity-at-least-value result)))
   ((list? result)
    (string-join (map arity-result->string result) " or "))))

(define-method value-class-match? ((env <environment>)
                                   (v <value>)
                                   klass)
  #f)

(define-method value-class-match? ((env <environment>)
                                   (v <value-unknown>)
                                   klass)
  #t)

(define-method value-class-match? ((env <environment>)
                                   (v <value-object>)
                                   (vobj <value-object>))
  (cond
   ((is-a? (object-of vobj) <class>)
    (if (eq? (object-of v) #f)
        ;; #fはvalue-unknownと同じものと見なす
        ;; TODO: #fはmaybe的な意味合いで使われることがよくあり、その場合フロー解析を
        ;; きちんと行わないと誤判定が多くなる。現状はフロー解析を全く行っていないため
        ;; 場当たり的にこのような対応を行っている。将来は書き直すべき。
        #t
        (is-a? (object-of v) (object-of vobj))))
   (else
      ;; TODO: この振る舞いは必要だけど、混乱を招きかねないので、あとで考え直す
    (equal? (object-of v) (object-of vobj)))))

(define-method value-class-match? ((env <environment>)
                                   (v <value-object>)
                                   (vobj <value-class>))
  (cond
   ((eq? (object-of v) #f)
    ;; #fはvalue-unknownと同じものと見なす
    ;; TODO: #fはmaybe的な意味合いで使われることがよくあり、その場合フロー解析を
    ;; きちんと行わないと誤判定が多くなる。現状はフロー解析を全く行っていないため
    ;; 場当たり的にこのような対応を行っている。将来は書き直すべき。
    #t)
   (else
    ;; TODO: 名前だけでチェックするのは本来正しくない
    (boolean (member (value-class-name vobj)
                     (map class-name
                          (class-precedence-list (class-of (object-of v)))))))))

(define-method value-class-match? ((env <environment>)
                                   (v <value-instance>)
                                   (vobj <value-object>))
  (if (is-a? (object-of vobj) <class>)
      (boolean (member (value-object (object-of vobj))
                       (value-class-precedence-list (value-class-of v))))
      (boolean (member (value-object (class-of (object-of vobj)))
                       (value-class-precedence-list (value-class-of v))))))

(define-method value-class-match? ((env <environment>)
                                   (v <value-instance>)
                                   (vclass <value-class>))
  (boolean (member vclass
                   (value-class-precedence-list (value-class-of v)))))

(define-method value-class-match? ((env <environment>)
                                   (v <value-lambda>)
                                   klass)
  (value-class-match? env (value-instance <procedure>) klass))

(define-method value-class-match? ((env <environment>)
                                   (v <value-generic>)
                                   klass)
  (value-class-match? env (value-instance <generic>) klass))

(define-method value-class-match? ((env <environment>)
                                   (v <value-class>)
                                   klass)
  (value-class-match? env (value-instance (value-metaclass-of v)) klass))

(define-method value-class-match? ((env <environment>)
                                   (v <value-promise>)
                                   klass)
  (value-class-match? env (value-instance <promise>) klass))

(define-method value-class-match? ((env <environment>)
                                   (v <value-union>)
                                   klass)
  (find values (map (cut value-class-match? env <> klass) (values-of v))))

(define-method value-class-match? ((env <environment>)
                                   (v <multi-value>)
                                   klass)
  (value-class-match? env (one-value v) klass))

(define-method value-class-match? ((env <environment>)
                                   (v <value>)
                                   (klass <value-union>))
  (find values (map (cut value-class-match? env v <>) (values-of klass))))

(define (procedure-name proc)
  (format "~s" proc))

(define-method call-make ((v <value-object>))
  (if (is-a? (object-of v) <class>)
      (value-instance (object-of v))
      ;; エラーにしてもいいかもしれない(一応makeは総称関数なので、分からないということにしておく)
      (next-method)))

(define-method call-make ((v <value-class>))
  (value-instance v))

(define-method call-make ((v <value>))
  (value-unknown))

(define-method call-make ((v <value-unknown>))
  (value-unknown))

(define-method call-make ((vu <value-union>))
  (value-union (map call-make (values-of vu))))

(define (call-procedure env proc args)
  ;; 総称関数はプログラム内でメソッドが追加されている可能性があり、組み込みのarity系の関数は
  ;; 使えないため、引数の個数のチェックは行わない
  ;; (不正なものは引数の型のマッチングではじかれるためチェックしなくても大丈夫)
  (if (or (is-a? proc <generic>)
          (procedure-arity-includes? proc (length args)))
      (cond
       ((hash-table-get proc-table proc #f)
        => (lambda (proc-type)
             (sync-procedure! proc-type)
             (let ((ret-type-maker-list (find-ret-types (procedure-name proc)
                                                        1
                                                        env
                                                        (arg-type-tree-of proc-type)
                                                        args)))
               (if (null? ret-type-maker-list)
                   (value-unknown)
                   (apply combine-value
                          (map (cut <> env args) ret-type-maker-list))))))
       (else
        (when (register-procedure-type-default proc)
          (call-procedure env proc args))))
      (begin
        (let ((arty (arity proc)))
          (if (null? arty)
              (print-errmsg "no applicable method for ~s" proc)
              (print-errmsg "wrong number of arguments: ~s requires ~a, but got ~a"
                            proc
                            (arity-result->string (arity proc))
                            (length args))))
        (value-unknown))))

(define-method call ((env <environment>) (val-proc <value>) (args <list>))
  (call-procedure env object-apply (cons val-proc args)))

(define-method call ((env <environment>) (val-proc <value-unknown>) (args <list>))
  (value-unknown))

(define-method call ((env <environment>) (val-proc <value-object>) (args <list>))
  (cond
   ((procedure? (object-of val-proc))
    (call-procedure env (object-of val-proc) args))
   ((eq? (object-of val-proc) #f)
    ;; #fはvalue-unknownと同じものと見なす
    ;; TODO: #fはmaybe的な意味合いで使われることがよくあり、その場合フロー解析を
    ;; きちんと行わないと誤判定が多くなる。現状はフロー解析を全く行っていないため
    ;; 場当たり的にこのような対応を行っている。将来は書き直すべき。
    (value-unknown))
   (else
    (next-method))))

(define-method call ((env <environment>) (val-proc <value-instance>) (args <list>))
  (if (or (value-class-match? env val-proc (value-object <procedure>))
          (value-class-match? env val-proc (value-object <generic>)))
      (value-unknown)
      (next-method)))

(define-method call ((env <environment>) (val-proc <value-lambda>) (args <list>))
  (let ((reqarg (reqarg-of val-proc))
        (optarg (optarg-of val-proc)))
    (cond
     ((and (= optarg 0) (not (= reqarg (length args))))
      (print-errmsg "wrong number of arguments: ~a requires ~a, but got ~a"
                    (x->string val-proc)
                    reqarg
                    (length args))
      (value-unknown))
     ((and (= optarg 1) (< (length args) reqarg))
      (print-errmsg "wrong number of arguments: ~a requires more than ~a, but got ~a"
                    (x->string val-proc)
                    reqarg
                    (length args))
      (value-unknown))
     (else
      (apply (value-maker-of val-proc) args)))))

(define-method call ((env <environment>) (val-proc <value-generic>) (args <list>))
  (let ((proc-type (procedure-type-of val-proc)))
    (sync-procedure! proc-type)
    (let ((ret-type-maker-list (find-ret-types (x->string val-proc)
                                               1
                                               env
                                               (arg-type-tree-of proc-type)
                                               args)))
      (if (null? ret-type-maker-list)
          (value-unknown)
          (apply combine-value
                 (map (cut <> env args) ret-type-maker-list))))))

(define-method call ((env <environment>) (val-proc <multi-value>) (args <list>))
  (call env (one-value val-proc) args))

(define-method call ((env <environment>) (val-proc <value-union>) (args <list>))
  (value-unknown))


;;
(define-method value-procedure? ((env <environment>) (v <value>))
  (let ((proc-type (hash-table-get proc-table object-apply #f)))
    (cond
     ((is-a? proc-type <procedure-type>)
      (sync-procedure! proc-type)
      ;; TODO: もっときれいに書き直す
      (boolean (find (lambda (node)
                       (value-class-match? env v (car node)))
                     (cdr (arg-type-tree-of proc-type)))))
     (else
      #f))))

(define-method value-procedure? ((env <environment>) (v <value-unknown>))
  #t)

(define-method value-procedure? ((env <environment>) (v <value-object>))
  (let ((obj (object-of v)))
    (cond
     ((procedure? obj)
      #t)
     (else
      (next-method)))))

(define-method value-procedure? ((env <environment>) (v <value-lambda>))
  #t)

(define-method value-procedure? ((env <environment>) (v <value-instance>))
  (boolean (memq (value-class-name (value-class-of v)) '(<procedure> <generic>))))
  
(define-method value-procedure? ((env <environment>) (v <value-generic>))
  #t)

;; procedure table
(define proc-table (make-hash-table 'eq?))

;; leaf := <procedure> or #f
;; type := <value-instance> or <value-class> or |...|
;; node := (type . node-list)
;; node-list := (leaf node ...)
(define (add-type-list! node-list ret-type type-list)
  (cond
   ((null? type-list)
    (set-car! node-list ret-type))
   ((assoc (car type-list) (cdr node-list))
    => (lambda (node)
         (add-type-list! (cdr node) ret-type (cdr type-list))))
   (else
    (let* ((node (cons (car type-list) (list #f)))
           (new-list (sort (cons node (cdr node-list))
                           (lambda (v1 v2)
                             (cond
                              ((eq? (car v1) '|...|)
                               #f)
                              ((list? (car v1))
                               #f)
                              (else
                               (boolean (member (car v2)
                                                (value-class-precedence-list (car v1))))))))))
      (set-cdr! node-list new-list)
      (add-type-list! (cdr node) ret-type (cdr type-list))))))

(define (ret-types node-list)
  (if (car node-list)
      (list (car node-list))
      '()))

(define (arg-type? node type)
  (if (and (list? (car node))
           (eq? (caar node) type))
      (cdar node)
      #f))

;; TODO: もっときれいに書き直し
(define (%find-ret-types proc-name arg-index env node-list arg-list)
  (define (get-cont-list node-list)
    (filter-map (lambda (node)
                  (cond
                   ((eq? (car node) '|...|)
                    (cut ret-types (cdr node)))
                   ((arg-type? node '&optional)
                    => (lambda (types)
                         (let loop ((i arg-index)
                                    (rest-types types)
                                    (rest-args arg-list))
                           (cond
                            ((null? rest-args)
                             (cut ret-types (cdr node)))
                            ((null? rest-types)
                             (print-errmsg "too many arguments for ~a"
                                           proc-name)
                             #f)
                            ((value-class-match? env (car rest-args) (car rest-types))
                             (loop (+ i 1) (cdr rest-types) (cdr rest-args)))
                            (else
                             (print-errmsg "wrong type for argument ~a for ~a: ~a required, but got ~a"
                                           i
                                           proc-name
                                           (value-class-name (car rest-types))
                                           (x->string (car rest-args)))
                             #f)))))
                   ((arg-type? node '&keyword)
                    => (lambda (kv-list)
                         (let loop ((rest arg-list))
                           (cond
                            ((null? rest)
                             (cut ret-types (cdr node)))
                            ((= (length rest) 1)
                             (print-errmsg "no value is specified for ~a"
                                           (x->string (car rest)))
                             #f)
                            ((and (is-a? (car rest) <value-object>)
                                  (get-keyword (object-of (car rest)) kv-list #f))
                             => (lambda (type)
                                  (if (value-class-match? env (cadr rest) type)
                                      (loop (cddr rest))
                                      (begin
                                        (print-errmsg "wrong type for keyword argument ~s for ~a: ~a required, but got ~a"
                                                      (car rest)
                                                      proc-name
                                                      (x->string type)
                                                      (x->string (cadr rest)))
                                        #f))))
                            (else
                             (print-errmsg "wrong keyword ~a for ~a"
                                           (x->string (car rest))
                                           proc-name)
                             #f)))))
                   ((arg-type? node '&rest)
                    => (lambda (types)
                         (let loop ((i arg-index)
                                    (rest-args arg-list))
                           (cond
                            ((null? rest-args)
                             (cut ret-types (cdr node)))
                            ((value-class-match? env (car rest-args) (car types))
                             (loop (+ i 1) (cdr rest-args)))
                            (else
                             (print-errmsg "wrong type for argument ~a for ~a: ~a required, but got ~a"
                                           i
                                           proc-name
                                           (x->string (car types))
                                           (x->string (car rest-args)))
                             #f)))))
                   ((null? arg-list)
                    (cut ret-types node-list))
                   ((arg-type? node '&procedure)
                    => (lambda _
                         (if (value-procedure? env (car arg-list))
                             (cut %find-ret-types proc-name (+ arg-index 1)
                                  env (cdr node) (cdr arg-list))
                             (begin
                               (print-errmsg "wrong type for argument ~a for ~a: an applicable object required, but got ~a"
                                             arg-index
                                             proc-name
                                             (x->string (car arg-list)))
                               #f))))
                   ((value-class-match? env (car arg-list) (car node))
                    (cut %find-ret-types proc-name (+ arg-index 1)
                         env (cdr node) (cdr arg-list)))
                   (else
                    (print-errmsg "wrong type for argument ~a of ~a: ~a required, but got ~a"
                                  arg-index
                                  proc-name
                                  (string-join (map (lambda (node)
                                                      (x->string 
                                                       (value-class-name (car node))))
                                                    (cdr node-list))
                                               " or ")
                                  (x->string (car arg-list)))
                    #f)))
                (cdr node-list)))
  (cond
   ((null? (cdr node-list))
    (if (null? arg-list)
        (ret-types node-list)
        '()))
   (else
    (let ((cont-list (get-cont-list node-list)))
      (append-map (cut <>) cont-list)))))

(define (find-ret-types proc-name arg-index env node-list arg-list)
  (with-pool-error-message (lambda ()
                             (%find-ret-types proc-name arg-index
                                              env node-list arg-list))
                           (lambda (result msg-list)
                             (receive (warn-list err-list)
                                 (partition (lambda (elt)
                                              (eq? (list-ref elt 0) 'warning))
                                            msg-list)
                               (for-each (cut apply print-msg <>) warn-list)
                               (when (null? result)
                                 (if (= (length err-list) 1)
                                     (apply print-msg (list-ref err-list 0))
                                     (print-errmsg
                                      "~a can't be applied with arguments (~a)"
                                      proc-name
                                      (string-join (map x->string arg-list) " "))))))))

(define (ret-type->value ret-type)
  (match ret-type
    (('or types ...)
     `(lambda (env args)
        (value-union (map (cut <> env args) (list ,@(map ret-type->value types))))))
    (('lambda args body ...)
     `(lambda ,args ,@body))
    ('unidentified
     `(lambda (env args)
        (value-unknown)))
    (('exec procp ret-type)
     `(lambda (env args)
        (cond
         ((every (cut is-a? <> <value-object>) args)
          (value-object (apply ,procp (map object-of args))))
         (else
          (,(ret-type->value ret-type) env args)))))
    (('type? pred klass)
     `(lambda (env args)
        (let ((v (list-ref args 0)))
          (cond
           ((is-a? v <value-object>)
            (value-object (,pred (object-of v))))
           ((is-a? v <value-instance>)
            (value-object (value-class-match? env v (value-object ,klass))))
           (else
            (value-instance <boolean>))))))
    ((types ...)
     `(lambda (env args)
        (multi-value (map (cut <> env args) (list ,@(map ret-type->value types))))))
    ('???
     `(lambda (env args)
        (value-unknown)))
    ((? symbol? klass)
     `(lambda (env args)
        (value-instance ,klass)))
    ((? number? v)
     `(lambda (env args)
        (value-object ,v)))
    ((? boolean? v)
     `(lambda (env args)
        (value-object ,v)))))

(define (arg-type->value arg-type)
  (match arg-type
    ('|...|
     ''|...|)
    (('&procedure)
     `(list '&procedure))
    (('&optional types ...)
     `(list '&optional ,@(map arg-type->value types)))
    (('&keyword kv-list ...)
     `(list '&keyword ,@(map (lambda (elt)
                               (if (keyword? elt)
                                   elt
                                   (arg-type->value elt)))
                             kv-list)))
    (('&rest type)
     `(list '&rest ,(arg-type->value type)))
    ('unidentified
     '(value-unknown))
    (('or types ...)
     `(value-union (list ,@(map arg-type->value types))))
    (else
     `(value-object ,arg-type))))

(define (print-tree tree)
  (format (standard-error-port)
          (let loop ((obj tree))
           (if (list? obj)
               (format "(~a)" (string-join (map loop obj) " "))
               (format "[~a]" (x->string obj)))))
  (newline (standard-error-port)))

(define-method add-specializer! ((proc-type <procedure-type>)
                                ret-type-maker
                                (arg-types <list>))
  (add-type-list! (arg-type-tree-of proc-type) ret-type-maker arg-types))

(define-method sync-procedure! ((proc-type <procedure-type>))
  (when (is-a? (procedure-of proc-type) <generic>)
    (let ((methods (methods-of proc-type))
          (new-methods (slot-ref (procedure-of proc-type) 'methods)))
      (for-each (lambda (method)
                  (unless (memq method methods)
                    (add-specializer! proc-type
                                      (lambda (env args)
                                        (value-unknown))
                                      (append (map (lambda (klass)
                                                     (value-object klass))
                                                   (slot-ref method 'specializers))
                                              (if (slot-ref method 'optional)
                                                  '(|...|)
                                                  '())))))
                new-methods)
      (set! (methods-of proc-type) new-methods)))
  proc-type)

(define (register-procedure-type proc-obj ret-type-maker arg-types)
  (hash-table-update! proc-table proc-obj
                      (lambda (proc-type)
                        (add-specializer! proc-type ret-type-maker arg-types)
                        proc-type)
                      (let ((proc-type (make <procedure-type>
                                         :procedure proc-obj
                                         :name proc-obj)))
                        (sync-procedure! proc-type)
                        proc-type)))

(define (register-procedure-type-default proc-obj)
  (hash-table-update! proc-table proc-obj
                      (lambda (proc-type)
                        (cond
                         ((is-a? proc-obj <procedure>)
                          (add-specializer! proc-type
                                            (lambda (env args)
                                              (value-unknown))
                                            (let ((a (arity proc-obj)))
                                              (if (arity-at-least? a)
                                                  (append (make-list (arity-at-least-value a) (value-object <top>)) '(|...|))
                                                  (make-list a (value-object <top>))))))
                         ((is-a? proc-obj <generic>)
                          (sync-procedure! proc-type)))
                        proc-type)
                      (make <procedure-type> :procedure proc-obj :name proc-obj))
  (hash-table-get proc-table proc-obj))

(define-macro (define-proc-type mod proc . specs)
  `(when (global-variable-bound? ',mod ',proc)
     ,(if (null? specs)
          `(register-procedure-type-default (with-module ,mod ,proc))
          (let loop ((rest specs)
                     (body '(begin)))
            (cond
             ((null? rest)
              (reverse body))
             (else
              (loop (cddr rest)
                    (cons `(register-procedure-type (with-module ,mod ,proc)
                                                    ,(ret-type->value (car rest))
                                                    (list
                                                     ,@(map arg-type->value (cadr rest))))
                          body))))))))


(define (sort-ret-type sort-proc env args)
  (cond
   ((and (= (length args) 2)
         (not (value-procedure? env (list-ref args 1))))
    (print-errmsg "wrong type for argument 2 of ~a: an applicable object required, but got ~a"
                  sort-proc
                  (list-ref args 1))
    (value-unknown))
   ((is-a? (list-ref args 0) <value-object>)
    (value-instance (class-of (object-of (list-ref args 0)))))
   ((is-a? (list-ref args 0) <value-instance>)
    (list-ref args 0))
   (else
    (value-unknown))))

;; 注意事項
;; (1) 引数で<pair>は使用しないこと。<pair>だと '() は含まれないが、<list>を戻り値として持つ
;;     関数を引数として使用すると型チェックでエラーとなってしまう。このこと自体は厳密には
;;     正しいが、実際には '() が返らないケースもあるためチェックを緩くしておく。

;; TODO: value-unionが引数に含まれる場合、もしかしたらobject-*が呼ばれる可能性が
;; あるためこの戻り値は厳密には誤り。
(define-proc-type scheme *
  (lambda (env args)
    (cond
     ((= (length args) 0)
      (value-object 1))
     ((member (value-unknown) args)
      (value-unknown))
     ((every (cut value-class-match? env <> (value-object <number>)) args)
      (value-number))
     (else
      (call-procedure env object-* args))))
    (...))
;; TODO: value-unionが引数に含まれる場合、もしかしたらobject-+が呼ばれる可能性が
;; あるためこの戻り値は厳密には誤り。
(define-proc-type scheme +
  (lambda (env args)
    (cond
     ((= (length args) 0)
      (value-object 1))
     ((member (value-unknown) args)
      (value-unknown))
     ((every (cut value-class-match? env <> (value-object <number>)) args)
      (value-number))
     (else
      (call-procedure env object-+ args))))
  (...))
;; TODO: value-unionが引数に含まれる場合、もしかしたらobject--が呼ばれる可能性が
;; あるためこの戻り値は厳密には誤り。
(define-proc-type scheme -
  (lambda (env args)
    (cond
     ((member (value-unknown) args)
      (value-unknown))
     ((every (cut value-class-match? env <> (value-object <number>)) args)
      (value-number))
     (else
      (call-procedure env object-- args))))
  (<top> ...))
;; TODO: value-unionが引数に含まれる場合、もしかしたらobject-/が呼ばれる可能性が
;; あるためこの戻り値は厳密には誤り。
(define-proc-type scheme /
    (lambda (env args)
    (cond
     ((member (value-unknown) args)
      (value-unknown))
     ((every (cut value-class-match? env <> (value-object <number>)) args)
      (value-number))
     (else
      (call-procedure env object-/ args))))
    (<top> ...))
(define-proc-type scheme < <boolean> (<top> <top> ...))
(define-proc-type scheme <= <boolean> (<top> <top> ...))
(define-proc-type scheme = <boolean> (<top> <top> ...))
(define-proc-type scheme > <boolean> (<top> <top> ...))
(define-proc-type scheme >= <boolean> (<top> <top> ...))
(define-proc-type scheme abs <real> (<number>))
(define-proc-type scheme acos <number> (<number>))
(define-proc-type scheme angle <real> (<number>))
(define-proc-type scheme append
  (lambda (env args)
    (cond
     ((null? args)
      (value-object '()))
     ((every (cut value-class-match? env <> (value-object <list>))
             (drop-right args 1))
      (value-instance <pair>))
     (else
      (print-errmsg "~a can't be applied with arguments (~a)"
                    append
                    (string-join (map x->string args) " "))
      (value-unknown))))
    (...))
(define-proc-type scheme apply ??? ((&procedure) ...))
(define-proc-type scheme asin <number> (<number>))
(define-proc-type scheme assoc (or <pair> #f) (<top> <list>))
(define-proc-type scheme assq (or <pair> #f) (<top> <list>))
(define-proc-type scheme assv (or <pair> #f) (<top> <list>))
(define-proc-type scheme atan <number> (<number> (&optional <number>)))
(define-proc-type scheme boolean? (type? boolean? <boolean>) (<top>))
(define-proc-type scheme caaaar ??? (<list>))
(define-proc-type scheme caaadr ??? (<list>))
(define-proc-type scheme caaar ??? (<list>))
(define-proc-type scheme caadar ??? (<list>))
(define-proc-type scheme caaddr ??? (<list>))
(define-proc-type scheme caadr ??? (<list>))
(define-proc-type scheme caar ??? (<list>))
(define-proc-type scheme cadaar ??? (<list>))
(define-proc-type scheme cadadr ??? (<list>))
(define-proc-type scheme cadar ??? (<list>))
(define-proc-type scheme caddar ??? (<list>))
(define-proc-type scheme cadddr ??? (<list>))
(define-proc-type scheme caddr ??? (<list>))
(define-proc-type scheme cadr ??? (<list>))
(define-proc-type scheme call-with-current-continuation ??? ((&procedure)))
(define-proc-type scheme call-with-input-file
  ??? (<string> (&procedure) (&keyword :if-does-not-exist (or :error
                                                              :create
                                                              #f)
                                       :buffering (or :full
                                                      :none
                                                      :line
                                                      :modest)
                                       :element-type (or :character
                                                         :binary)
                                       :encoding (or <string> <symbol>)
                                       :conversion-buffer-size <integer>)))
(define-proc-type scheme call-with-output-file
  ??? (<string> (&procedure) (&keyword :if-does-not-exist (or :error
                                                              :create
                                                              #f)
                                       :if-exists (or :supersede
                                                      :append
                                                      :overwrite
                                                      :error
                                                      #f)
                                       :mode <integer>
                                       :buffering (or :full
                                                      :none
                                                      :line)
                                       :element-type (or :character
                                                         :binary)
                                       :encoding (or <string> <symbol>)
                                       :conversion-buffer-size <integer>)))
(define-proc-type scheme call-with-values ??? ((&procedure) (&procedure)))
(define-proc-type scheme call/cc ??? ((&procedure)))
(define-proc-type scheme car ??? (<list>))
(define-proc-type scheme cdaaar ??? (<list>))
(define-proc-type scheme cdaadr ??? (<list>))
(define-proc-type scheme cdaar ??? (<list>))
(define-proc-type scheme cdadar ??? (<list>))
(define-proc-type scheme cdaddr ??? (<list>))
(define-proc-type scheme cdadr ??? (<list>))
(define-proc-type scheme cdar ??? (<list>))
(define-proc-type scheme cddaar ??? (<list>))
(define-proc-type scheme cddadr ??? (<list>))
(define-proc-type scheme cddar ??? (<list>))
(define-proc-type scheme cdddar ??? (<list>))
(define-proc-type scheme cddddr ??? (<list>))
(define-proc-type scheme cdddr ??? (<list>))
(define-proc-type scheme cddr ??? (<list>))
(define-proc-type scheme cdr ??? (<list>))
(define-proc-type scheme ceiling <real> (<real>))
(define-proc-type scheme char->integer <integer> (<char>))
(define-proc-type scheme char-alphabetic? <boolean> (<char>))
(define-proc-type scheme char-ci<=? <boolean> (<char> <char>))
(define-proc-type scheme char-ci<? <boolean> (<char> <char>))
(define-proc-type scheme char-ci=? <boolean> (<char> <char>))
(define-proc-type scheme char-ci>=? <boolean> (<char> <char>))
(define-proc-type scheme char-ci>? <boolean> (<char> <char>))
(define-proc-type scheme char-downcase <char> (<char>))
(define-proc-type scheme char-lower-case? <boolean> (<char>))
(define-proc-type scheme char-numeric? <boolean> (<char>))
(define-proc-type scheme char-ready? <boolean> (<port>))
(define-proc-type scheme char-upcase <char> (<char>))
(define-proc-type scheme char-upper-case? <boolean> (<char>))
(define-proc-type scheme char-whitespace? <boolean> (<char>))
(define-proc-type scheme char<=? <boolean> (<char> <char>))
(define-proc-type scheme char<? <boolean> (<char> <char>))
(define-proc-type scheme char=? <boolean> (<char> <char>))
(define-proc-type scheme char>=? <boolean> (<char> <char>))
(define-proc-type scheme char>? <boolean> (<char> <char>))
(define-proc-type scheme char? <boolean> (<top>))
(define-proc-type scheme close-input-port ??? (<port>))
(define-proc-type scheme close-output-port ??? (<port>))
(define-proc-type scheme complex? (type? complex? <complex>) (<top>))
(define-proc-type scheme cons <pair> (<top> <top>))
(define-proc-type scheme cos <number> (<number>))
(define-proc-type scheme current-input-port <port> ((&optional <port>)))
(define-proc-type scheme current-output-port <port> ((&optional <port>)))
(define-proc-type scheme denominator <real> (<number>))
(define-proc-type scheme display <undefined-object> (<top> (&optional <port>)))
(define-proc-type scheme dynamic-wind ??? ((&procedure) (&procedure) (&procedure)))
(define-proc-type scheme eof-object? <boolean> (<top>))
(define-proc-type scheme eq? (exec eq? <boolean>) (<top> <top>))
(define-proc-type scheme equal? <boolean> (<top> <top>))
(define-proc-type scheme eqv? (exec eq? <boolean>) (<top> <top>))
(define-proc-type scheme eval ??? (<top> <module>))
(define-proc-type scheme even? <boolean> (<integer>))
(define-proc-type scheme exact->inexact <number> (<number>))
(define-proc-type scheme exact? <boolean> (<top>))
(define-proc-type scheme exp <number> (<number>))
(define-proc-type scheme expt <number> (<number> <number>))
(define-proc-type scheme floor <real> (<real>))
(define-proc-type scheme for-each
  <undefined-object> ((&procedure) <sequence> (&rest <sequence>)))
(define-proc-type scheme force (lambda (env args)
                                 (force-value (list-ref args 0)))
  (<top>))
(define-proc-type scheme gcd <real> (<real> (&rest <real>)))
(define-proc-type scheme imag-part <real> (<number>))
(define-proc-type scheme inexact->exact (or <integer> <rational>) (<number>))
(define-proc-type scheme inexact? <boolean> (<top>))
(define-proc-type scheme input-port? <boolean> (<top>))
(define-proc-type scheme integer->char <char> (<integer>))
(define-proc-type scheme integer? (type? integer? <integer>) (<top>))
(define-proc-type scheme interaction-environment <module> ())
(define-proc-type scheme lcm <real> (<real> (&rest <real>)))
(define-proc-type scheme length <integer> (<list>))
(define-proc-type scheme length+ (or <integer> #f) (<list>))
(define-proc-type scheme list <list> (...))
(define-proc-type scheme list->string <string> (<list>))
(define-proc-type scheme list->vector
  <vector> (<list> (&optional <integer> <integer>)))
(define-proc-type scheme list-ref ??? (<list> <integer> (&optional <top>)))
(define-proc-type scheme list-tail ??? (<list> <integer> (&optional <top>)))
(define-proc-type scheme list? (type? list? <list>) (<top>))
(define-proc-type scheme load
  (lambda (env args)
    (let ((filename-arg (list-ref args 0)))
      (when (is-a? filename-arg <value-object>)
        (let ((filename (object-of filename-arg))
              (paths (or (and-let* ((rest (member (value-object :paths) (cdr args))))
                           (cadr rest))
                         #f)))
          (cond
           ((or (string-prefix? "/" filename)
                (string-prefix? "./" filename)
                (string-prefix? "../" filename))
            ((with-module codecheck scan-file) env filename))
           (paths
            (when (and (is-a? paths <value-object>)
                       (list? (object-of paths))
                       (every string? (object-of paths)))
              (let ((fn (find file-is-readable? (map (cut build-path <> filename)
                                                     (object-of paths)))))
                ((with-module codecheck scan-file) env fn))))
           (else
            ((with-module codecheck scan-file) env
             (or (find file-is-readable? (map (cut build-path <> filename)
                                              *load-path*))
                 filename)))))))
    (value-unknown))
  (<string> (&keyword :paths (or <list> #f)
                      :error-if-not-found <boolean>
                      :environment (or <module> #f))))
(define-proc-type scheme load-from-port
  ??? (<port> (&keyword :environment (or <module> #f))))
(define-proc-type scheme log <number> (<number>))
(define-proc-type scheme magnitude <real> (<number>))
(define-proc-type scheme make-polar <complex> (<real> <real>))
(define-proc-type scheme make-rectangular <complex> (<real> <real>))
(define-proc-type scheme make-string <string> (<integer> <char>))
(define-proc-type scheme make-vector <vector> (<integer> (&optional <top>)))
(define-proc-type scheme map <list> ((&procedure) <sequence> (&rest <sequence>)))
(define-proc-type scheme max
  <integer> (<integer> (&rest <integer>))
  <rational> (<rational> (&rest <rational>))
  <real> (<real> (&rest <real>)))
(define-proc-type scheme member (or <list> #f) (<top> <list>))
(define-proc-type scheme memq (exec memq (or <list> #f)) (<top> <list>))
(define-proc-type scheme memv (exec memv (or <list> #f)) (<top> <list>))
(define-proc-type scheme min
  <integer> (<integer> (&rest <integer>))
  <rational> (<rational> (&rest <rational>))
  <real> (<real> (&rest <real>)))
(define-proc-type scheme modulo <integer> (<integer> <integer>))
(define-proc-type scheme nearly=? <boolean> (<number> <number> <number>))
(define-proc-type scheme negative? <boolean> (<real>))
(define-proc-type scheme newline <undefined-object> ((&optional <port>)))
(define-proc-type scheme not
  (lambda (env args)
    (let ((arg (list-ref args 0)))
      (cond
       ((equal? (value-object #f) arg)
        (value-object #t))
       ((equal? (value-instance <boolean>) arg)
        (value-instance <boolean>))
       ((any (cut is-a? arg <>) (list <multi-value> <value-union>))
        (value-instance <boolean>))
       (else
        (value-object #f)))))
    (<top>))
(define-proc-type scheme null-environment <module> (<integer>))
(define-proc-type scheme null? (type? null? <null>) (<top>))
(define-proc-type scheme number->string
  (or <string> <number>) (<number> (&optional <integer> <boolean>)))
(define-proc-type scheme number? (type? number? <number>) (<top>))
(define-proc-type scheme numerator <number> (<number>))
(define-proc-type scheme odd? <boolean> (<integer>))
(define-proc-type scheme open-input-file
  <port> (<string> (&keyword :if-does-not-exist (or :error
                                                    :create
                                                    #f)
                             :buffering (or :full
                                            :none
                                            :line
                                            :modest)
                             :element-type (or :character
                                               :binary)
                             :encoding (or <string> <symbol>)
                             :conversion-buffer-size <integer>)))
(define-proc-type scheme open-output-file
  <port> (<string> (&keyword :if-does-not-exist (or :error
                                                    :create
                                                    #f)
                             :if-exists (or :supersede
                                            :append
                                            :overwrite
                                            :error
                                            #f)
                             :mode <integer>
                             :buffering (or :full
                                            :none
                                            :line)
                             :element-type (or :character
                                               :binary)
                             :encoding (or <string> <symbol>)
                             :conversion-buffer-size <integer>)))
(define-proc-type scheme output-port? <boolean> (<top>))
(define-proc-type scheme pair? (type? pair? <pair>) (<top>))
(define-proc-type scheme peek-char (or <char> <eof-object>) ((&optional <port>)))
(define-proc-type scheme port? (type? port? <port>) (<top>))
(define-proc-type scheme positive? <boolean> (<real>))
(define-proc-type scheme procedure?
  (lambda (env args)
    (cond
     ((is-a? (list-ref args 0) <value-unknown>)
      (value-instance <boolean>))
     (else
      (value-procedure? env (list-ref args 0)))))
  (<top>))
(define-proc-type scheme quotient <integer> (<integer> <integer>))
(define-proc-type scheme rational? (type? rational? <rational>) (<top>))
(define-proc-type scheme read ??? ((&optional <port>)))
(define-proc-type scheme read-char (or <char> <eof-object>) ((&optional <port>)))
(define-proc-type scheme real-part <real> (<number>))
(define-proc-type scheme real? (type? real? <real>) (<top>))
(define-proc-type scheme remainder <integer> (<integer> <integer>))
(define-proc-type scheme reverse <list> (<list>))
(define-proc-type scheme round <real> (<real>))
(define-proc-type scheme scheme-report-environment <module> (<integer>))
(define-proc-type scheme set-car! ??? (<list> <top>))
(define-proc-type scheme set-cdr! ??? (<list> <top>))
(define-proc-type scheme sin <number> (<number>))
(define-proc-type scheme sqrt <number> (<number>))
(define-proc-type scheme string <string> ((&rest <char>)))
(define-proc-type scheme string->list <list> (<string> (&optional <integer> <integer>)))
(define-proc-type scheme string->number
  (or <integer> <rational> <real> <complex> #f) (<string> (&optional <integer>)))
(define-proc-type scheme string->symbol <symbol> (<string>))
(define-proc-type scheme string-append <string> ((&rest <string>)))
(define-proc-type scheme string-ci<=? <boolean> (<string> <string>))
(define-proc-type scheme string-ci<? <boolean> (<string> <string>))
(define-proc-type scheme string-ci=? <boolean> (<string> <string>))
(define-proc-type scheme string-ci>=? <boolean> (<string> <string>))
(define-proc-type scheme string-ci>? <boolean> (<string> <string>))
(define-proc-type scheme string-copy
  <string> (<string> (&optional <integer> <integer>)))
(define-proc-type scheme string-length <integer> (<string>))
(define-proc-type scheme string-ref ??? (<string> <integer> (&optional <top>)))
(define-proc-type scheme string-set! <string> (<string> <integer> <char>))
(define-proc-type scheme string<=? <boolean> (<string> <string>))
(define-proc-type scheme string<? <boolean> (<string> <string>))
(define-proc-type scheme string=? <boolean> (<string> <string>))
(define-proc-type scheme string>=? <boolean> (<string> <string>))
(define-proc-type scheme string>? <boolean> (<string> <string>))
(define-proc-type scheme string? (type? string? <string>) (<top>))
(define-proc-type scheme substring <string> (<string> <integer> <integer>))
(define-proc-type scheme symbol->string <string> (<symbol>))
(define-proc-type scheme symbol? (type? symbol? <symbol>) (<top>))
(define-proc-type scheme tan <number> (<number>))
(define-proc-type scheme truncate <real> (<real>))
(define-proc-type scheme values (lambda (env args)
                                  (multi-value (map one-value args)))
  (...))
(define-proc-type scheme vector <vector> (...))
(define-proc-type scheme vector->list
  <list> (<vector> (&optional <integer> <integer>)))
(define-proc-type scheme vector-fill!
  <undefined-object> (<vector> <top> (&optional <integer> <integer>)))
(define-proc-type scheme vector-length <integer> (<vector>))
(define-proc-type scheme vector-ref ??? (<vector> <integer> (&optional <top>)))
(define-proc-type scheme vector-set! <undefined-object> (<vector> <integer> <top>))
(define-proc-type scheme vector? (type? vector? <vector>) (<top>))
(define-proc-type scheme with-input-from-file
  ??? (<string> (&procedure) (&keyword :if-does-not-exist (or :error
                                                              :create
                                                              #f)
                                       :buffering (or :full
                                                      :none
                                                      :line
                                                      :modest)
                                       :element-type (or :character
                                                         :binary)
                                       :encoding (or <string> <symbol>)
                                       :conversion-buffer-size <integer>)))
(define-proc-type scheme with-output-to-file
  ??? (<string> (&procedure) (&keyword :if-does-not-exist (or :error
                                                              :create
                                                              #f)
                                       :if-exists (or :supersede
                                                      :append
                                                      :overwrite
                                                      :error
                                                      #f)
                                       :mode <integer>
                                       :buffering (or :full
                                                      :none
                                                      :line)
                                       :element-type (or :character
                                                         :binary)
                                       :encoding (or <string> <symbol>)
                                       :conversion-buffer-size <integer>)))
(define-proc-type scheme write <undefined-object> (<top> (&optional <port>)))
(define-proc-type scheme write-char <undefined-object> (<char> (&optional <port>)))
(define-proc-type scheme zero? <boolean> (<real>))


(define-proc-type gauche *. <number> ((&rest <number>)))
(define-proc-type gauche +. <number> ((&rest <number>)))
(define-proc-type gauche -. <number> (<number> (&rest <number>)))
(define-proc-type gauche /. <number> (<number> (&rest <number>)))
(define-proc-type gauche acons <pair> (<top> <top> <top>))
(define-proc-type gauche acosh <number> (<number>))
(define-proc-type gauche alist->tree-map <tree-map> (<list> (&procedure) (&procedure)))
(define-proc-type gauche all-modules <list> ())
;(define-proc-type gauche allocate-instance <top> (<top>))
(define-proc-type gauche any ??? ((&procedure) <list> (&rest <list>)))
(define-proc-type gauche any$ <procedure> ((&procedure)))
;; TODO: &restの中に&procedureが書けないのをなんとかする
(define-proc-type gauche any-pred
  (lambda (env args)
    (cond
     ((every (cut value-procedure? env <>) args)
      (value-instance <procedure>))
     (else
      (print-errmsg "~a can't be applied with arguments (~a)"
                    any-pred
                    (string-join (map x->string args) " "))
      (value-unknown))))
  (...))
(define-proc-type gauche append! 
  (lambda (env args)
    (cond
     ((null? args)
      (value-object '()))
     ((every (cut value-class-match? env <> (value-object <list>))
             (drop-right args 1))
      (value-instance <pair>))
     (else
      (print-errmsg "~a can't be applied with arguments (~a)"
                    append
                    (string-join (map x->string args) " "))
      (value-unknown))))
  (...))
(define-proc-type gauche apply$ <procedure> ((&procedure)))
;(define-proc-type gauche apply-generic <top> (<top>))
;(define-proc-type gauche apply-method <top> (<top>))
;(define-proc-type gauche apply-methods <top> (<top>))
(define-proc-type gauche arity (or <integer> <arity-at-least> <list>) ((&procedure)))
(define-proc-type gauche arity-at-least-value <integer> (<arity-at-least>))
(define-proc-type gauche arity-at-least? (type? arity-at-least? <arity-at-least>) (<top>))
(define-proc-type gauche ash <integer> (<integer> <integer>))
(define-proc-type gauche asinh <number> (<number>))
(define-proc-type gauche assoc$ <procedure> (<top>))
(define-proc-type gauche atanh <number> (<number>))
(define-proc-type gauche bignum? <boolean> (<top>))
(define-proc-type gauche bit-field <integer> (<integer> <integer> <integer>))
(define-proc-type gauche boolean <boolean> (<top>))
(define-proc-type gauche byte-ready? <boolean> (<port>))
(define-proc-type gauche caaaar ??? (<list>))
(define-proc-type gauche caaadr ??? (<list>))
(define-proc-type gauche caaar ??? (<list>))
(define-proc-type gauche caadar ??? (<list>))
(define-proc-type gauche caaddr ??? (<list>))
(define-proc-type gauche caadr ??? (<list>))
(define-proc-type gauche cadaar ??? (<list>))
(define-proc-type gauche cadadr ??? (<list>))
(define-proc-type gauche cadar ??? (<list>))
(define-proc-type gauche caddar ??? (<list>))
(define-proc-type gauche cadddr ??? (<list>))
(define-proc-type gauche caddr ??? (<list>))
(define-proc-type gauche call-with-input-string ??? (<string> (&procedure)))
(define-proc-type gauche call-with-output-string <string> ((&procedure)))
(define-proc-type gauche call-with-string-io <string> (<string> (&procedure)))
(define-proc-type gauche cdaaar ??? (<list>))
(define-proc-type gauche cdaadr ??? (<list>))
(define-proc-type gauche cdaar ??? (<list>))
(define-proc-type gauche cdadar ??? (<list>))
(define-proc-type gauche cdaddr ??? (<list>))
(define-proc-type gauche cdadr ??? (<list>))
(define-proc-type gauche cddaar ??? (<list>))
(define-proc-type gauche cddadr ??? (<list>))
(define-proc-type gauche cddar ??? (<list>))
(define-proc-type gauche cdddar ??? (<list>))
(define-proc-type gauche cddddr ??? (<list>))
(define-proc-type gauche cdddr ??? (<list>))
(define-proc-type gauche ceiling->exact <rational> (<real>))
;(define-proc-type gauche change-class <top> (<top>))
;(define-proc-type gauche change-object-class <top> (<top>))
(define-proc-type gauche char->ucs (or <integer> #f) (<char>))
(define-proc-type gauche char-set <char-set> ((&rest <char>)))
(define-proc-type gauche char-set-contains? <boolean> (<char-set> <char>))
(define-proc-type gauche char-set-copy <char-set> (<char-set>))
(define-proc-type gauche char-set? (type? char-set? <char-set>) (<top>))
(define-proc-type gauche circular-list? <boolean> (<top>))
(define-proc-type gauche clamp <real> (<real>))
(define-proc-type gauche class-direct-methods <list> (<class>))
(define-proc-type gauche class-direct-slots <list> (<class>))
(define-proc-type gauche class-direct-subclasses <list> (<class>))
(define-proc-type gauche class-direct-supers <list> (<class>))
(define-proc-type gauche class-name <symbol> (<class>))
(define-proc-type gauche class-of <class> (<top>))
(define-proc-type gauche class-precedence-list <list> (<class>))
;(define-proc-type gauche class-redefinition <top> (<top>))
;(define-proc-type gauche class-slot-accessor <top> (<top>))
(define-proc-type gauche class-slot-bound? <boolean> (<class> <symbol>))
;(define-proc-type gauche class-slot-definition <top> (<top>))
(define-proc-type gauche class-slot-ref ??? (<class> <symbol>))
(define-proc-type gauche class-slot-set! ??? (<class> <symbol> <top>))
(define-proc-type gauche class-slots <list> (<class>))
(define-proc-type gauche closure? <boolean> (<top>))
(define-proc-type gauche compare <integer> (<top> <top>))
(define-proc-type gauche complement <procedure> ((&procedure)))
(define-proc-type gauche compose 
  (lambda (env args)
    (cond
     ((every (cut value-procedure? env <>) args)
      (value-instance <procedure>))
     (else
      (print-errmsg "~a can't be applied with arguments (~a)"
                    compose
                    (string-join (map x->string args) " "))
      (value-unknown))))
  (...))
;(define-proc-type gauche compute-applicable-methods <top> (<top>))
;(define-proc-type gauche compute-cpl <top> (<top>))
;(define-proc-type gauche compute-get-n-set <top> (<top>))
;(define-proc-type gauche compute-slot-accessor <top> (<top>))
;(define-proc-type gauche compute-slots <top> (<top>))
(define-proc-type gauche condition-has-type? <boolean> (<top> <top>))
(define-proc-type gauche condition-ref ??? (<condition> <symbol>))
(define-proc-type gauche condition-type? <boolean> (<top>))
(define-proc-type gauche condition? (type? condition? <condition>) (<top>))
(define-proc-type gauche copy-bit <integer> (<integer> <integer> <boolean>))
(define-proc-type gauche copy-bit-field <integer> (<integer> <integer> <integer> <integer>))
(define-proc-type gauche copy-port ??? (<port> <port> (&keyword :unit
                                                                (or <integer>
                                                                    'byte
                                                                    'char)
                                                                :size <integer>)))
(define-proc-type gauche cosh <number> (<number>))
(define-proc-type gauche count$ <procedure> ((&procedure)))
(define-proc-type gauche current-class-of <class> (<top>))
(define-proc-type gauche current-error-port <port> ())
(define-proc-type gauche current-exception-handler <procedure> ())
(define-proc-type gauche current-load-history <list> ())
(define-proc-type gauche current-load-next <list> ())
(define-proc-type gauche current-load-port <port> ())
(define-proc-type gauche current-thread <thread> ())
(define-proc-type gauche current-time <time> ())
;(define-proc-type gauche debug-source-info ??? (<top>))
(define-proc-type gauche decode-float <vector> (<real>))
(define-proc-type gauche define-reader-ctor ??? (<symbol> (&procedure)))
(define-proc-type gauche delete$ <procedure> ((&procedure)))
(define-proc-type gauche delete-keyword <list> (<keyword> <list>))
(define-proc-type gauche delete-keyword! <list> (<keyword> <list>))
;(define-proc-type gauche delete-method! <top> (<top>))
(define-proc-type gauche digit->integer <integer> (<char> (&optional <integer>)))
;(define-proc-type gauche disasm <top> (<top>))
(define-proc-type gauche dotted-list? <boolean> (<top>))
(define-proc-type gauche dynamic-load ??? (<string> (&keyword :init-function <string>
                                                              :export-symbols <boolean>)))
(define-proc-type gauche eager (lambda (env args)
                                 (list-ref args 0))
  (<top>))
(define-proc-type gauche eq-hash <integer> (<top>))
(define-proc-type gauche eqv-hash <integer> (<top>))
;(define-proc-type gauche error ??? (<string> <top> ...))
;(define-proc-type gauche errorf <top> (<top>))
(define-proc-type gauche every$ <procedure> ((&procedure)))
(define-proc-type gauche every-pred
  (lambda (env args)
    (cond
     ((every (cut value-procedure? env <>) args)
      (value-instance <procedure>))
     (else
      (print-errmsg "~a can't be applied with arguments (~a)"
                    every-pred
                    (string-join (map x->string args) " "))
      (value-unknown))))
  (...))
(define-proc-type gauche exit ??? ((&optional <integer>)))
;(define-proc-type gauche extract-condition <top> (<top>))
(define-proc-type gauche file-exists? <boolean> (<string>))
(define-proc-type gauche file-is-directory? <boolean> (<string>))
(define-proc-type gauche file-is-regular? <boolean> (<string>))
(define-proc-type gauche filter$ <procedure> ((&procedure)))
(define-proc-type gauche find ??? ((&procedure) <sequence> (&rest <sequence>)))
(define-proc-type gauche find$ <procedure> ((&procedure)))
(define-proc-type gauche find-module (or <module> #f) (<symbol>))
(define-proc-type gauche find-tail$ <procedure> ((&procedure)))
(define-proc-type gauche fixnum? <boolean> (<top>))
(define-proc-type gauche flonum? <boolean> (<top>))
(define-proc-type gauche floor->exact <integer> (<real>))
(define-proc-type gauche flush <undefined-object> ((&optional <port>)))
(define-proc-type gauche flush-all-ports <undefined-object> ())
(define-proc-type gauche fmod <real> (<real> <real>))
(define-proc-type gauche fold ??? ((&procedure) <top> <sequence> (&rest <sequence>)))
;(define-proc-type gauche fold$ <procedure> (<top>))
;(define-proc-type gauche fold-right <top> (<top>))
(define-proc-type gauche fold-right$ <procedure> ((&procedure) <top>))
(define-proc-type gauche for-each$ <procedure> ((&procedure)))
;(define-proc-type gauche foreign-pointer-attribute-get <top> (<top>))
;(define-proc-type gauche foreign-pointer-attribute-set <top> (<top>))
;(define-proc-type gauche foreign-pointer-attributes <top> (<top>))
(define-proc-type gauche format
  <string> (<string> (&rest <top>))
  (or <undefined-object> <string>) ((or <port> <boolean>) <string> (&rest <top>)))
(define-proc-type gauche format/ss
  <string> (<string> (&rest <top>))
  (or <undefined-object> <string>) ((or <port> <boolean>) <string> (&rest <top>)))
(define-proc-type gauche frexp (<real> <real>) (<top>))
(define-proc-type gauche gauche-architecture <string> ())
(define-proc-type gauche gauche-architecture-directory <string> ())
(define-proc-type gauche gauche-character-encoding <symbol> ())
(define-proc-type gauche gauche-dso-suffix <string> ())
(define-proc-type gauche gauche-library-directory <string> ())
(define-proc-type gauche gauche-site-architecture-directory <string> ())
(define-proc-type gauche gauche-site-library-directory <string> ())
(define-proc-type gauche gauche-version <string> ())
;(define-proc-type gauche gc <top> (<top>))
(define-proc-type gauche gc-stat <list> ())
(define-proc-type gauche gensym <symbol> ((&optional (or <string> #f))))
(define-proc-type gauche get-keyword ??? (<keyword> <list> (&optional <top>)))
;(define-proc-type gauche get-output-byte-string <top> (<top>))
(define-proc-type gauche get-output-string <string> (<port>))
(define-proc-type gauche get-remaining-input-string <string> (<port>))
(define-proc-type gauche get-signal-handler <procedure> (<integer>))
(define-proc-type gauche get-signal-handler-mask <procedure> (<integer>))
(define-proc-type gauche get-signal-handlers <list> ())
(define-proc-type gauche get-signal-pending-limit <integer> ())
(define-proc-type gauche getter-with-setter <procedure> ((&procedure) (&procedure)))
;; TODO: &keywordで&procedureの指定ができるようにする
(define-proc-type gauche glob <list> (<string> (&keyword :separator <char-set>
                                                         :folder <top>)))
(define-proc-type gauche glob-fold ??? (<string> (&procedure) <top>
                                                 (&keyword :separator <char-set>
                                                           :folder <top>)))
(define-proc-type gauche global-variable-bound? <boolean> (<top> <symbol>))
(define-proc-type gauche global-variable-ref ??? (<top> <symbol> (&optional <top>)))
(define-proc-type gauche has-setter? <boolean> ((&procedure)))
(define-proc-type gauche hash <integer> (<top>))
(define-proc-type gauche hash-table <hash-table> ((or 'eq? 'eqv? 'equal? 'string=?) (&rest <pair>)))
(define-proc-type gauche hash-table-clear! <undefined-object> (<hash-table>))
(define-proc-type gauche hash-table-delete! <boolean> (<hash-table> <top>))
(define-proc-type gauche hash-table-exists? <boolean> (<hash-table> <top>))
(define-proc-type gauche hash-table-fold ??? (<hash-table> (&procedure) <top>))
(define-proc-type gauche hash-table-for-each
  <undefined-object> (<hash-table> (&procedure)))
(define-proc-type gauche hash-table-get ??? (<hash-table> <top> (&optional <top>)))
(define-proc-type gauche hash-table-keys <list> (<hash-table>))
(define-proc-type gauche hash-table-map <list> (<hash-table> (&procedure)))
(define-proc-type gauche hash-table-num-entries <integer> (<hash-table>))
(define-proc-type gauche hash-table-pop! ??? (<hash-table> <top> (&optional <top>)))
(define-proc-type gauche hash-table-push! ??? (<hash-table> <top> (&optional <top>)))
(define-proc-type gauche hash-table-put! ??? (<hash-table> <top> <top>))
;(define-proc-type gauche hash-table-stat <top> (<top>))
(define-proc-type gauche hash-table-type <symbol> (<hash-table>))
(define-proc-type gauche hash-table-update!
  ??? (<hash-table> <top> (&procedure) (&optional <top>)))
(define-proc-type gauche hash-table-values <list> (<hash-table>))
(define-proc-type gauche hash-table? (type? hash-table? <hash-table>) (<top>))
(define-proc-type gauche identifier->symbol <identifier> (<symbol>))
(define-proc-type gauche identifier? (type? identifier? <identifier>) (<top>))
(define-proc-type gauche identity ??? (<top>))
(define-proc-type gauche inexact-/ <number> (<number> (&rest <number>)))
;(define-proc-type gauche initialize <top> (<top>))
;(define-proc-type gauche instance-slot-ref <top> (<top>))
;(define-proc-type gauche instance-slot-set <top> (<top>))
(define-proc-type gauche integer->digit
  (or <char> #f) (<integer> (&optional <integer>)))
(define-proc-type gauche integer-length <integer> (<integer>))
;; TODO: ちょっといいかげんなつくりなので、書き直す
(define-proc-type gauche is-a?
  (lambda (env args)
    (let ((v (list-ref args 0))
          (klass (list-ref args 1)))
      (cond
       ((is-a? v <value-unknown>)
        (value-instance <boolean>))
       ((and (is-a? klass <value-object>)
             (is-a? (object-of klass) <class>))
        (value-object (apply value-class-match? env args)))
       ((is-a? klass <value-class>)
        (value-object (apply value-class-match? env args)))
       (else
        (value-instance <boolean>)))))
  (<top> <class>))
(define-proc-type gauche keyword->string <string> (<keyword>))
(define-proc-type gauche keyword? (type? keyword? <keyword>) (<top>))
(define-proc-type gauche last-pair <pair> (<list>))
(define-proc-type gauche ldexp <real> (<real> <real>))
(define-proc-type gauche library-exists?
  <boolean> ((or <symbol> <string>) (&keyword :paths (or <list> #f)
                                              :force-search? <boolean>
                                              :strict? <boolean>)))
(define-proc-type gauche library-fold
  ??? ((or <symbol> <string>) (&procedure) <top> (&keyword :paths (or <list> #f)
                                                           :force-search? <boolean>
                                                           :strict? <boolean>
                                                           :allow-duplicates? <boolean>)))
(define-proc-type gauche library-for-each
  ??? ((or <symbol> <string>) (&procedure) (&keyword :paths (or <list> #f)
                                                     :strict? <boolean>
                                                     :allow-duplicates? <boolean>)))
(define-proc-type gauche library-has-module? <boolean> (<string> <symbol>))
(define-proc-type gauche library-map
  <list> ((or <symbol> <string>) (&procedure) (&keyword :paths (or <list> #f)
                                                     :strict? <boolean>
                                                     :allow-duplicates? <boolean>)))
(define-proc-type gauche list* ??? (...))
(define-proc-type gauche list->sys-fdset <sys-fdset> (<list>))
(define-proc-type gauche list-copy <list> (<list>))
(define-proc-type gauche logand <integer> (<integer> (&rest <integer>)))
(define-proc-type gauche logbit? <boolean> (<integer> <integer>))
(define-proc-type gauche logcount <integer> (<integer>))
(define-proc-type gauche logior <integer> (<integer> (&rest <integer>)))
(define-proc-type gauche lognot <integer> (<integer>))
(define-proc-type gauche logtest <boolean> (<integer> (&rest <integer>)))
(define-proc-type gauche logxor <integer> (<integer> (&rest <integer>)))
;(define-proc-type gauche macroexpand <top> (<top>))
;(define-proc-type gauche macroexpand-1 <top> (<top>))
(define-proc-type gauche make (lambda (env args)
                                (call-make (one-value (car args))))
  (<class> ...))
(define-proc-type gauche make-byte-string <string> (<integer> (&optional <integer>)))
(define-proc-type gauche make-compound-condition
  <compound-condition> ((&rest <condition>)))
(define-proc-type gauche make-condition <condition> (<condition-meta> ...))
(define-proc-type gauche make-condition-type <condition-meta> (<symbol> <condition-meta> <list>))
(define-proc-type gauche make-hash-table <hash-table> ((&optional (or 'eq? 'eqv? 'equal? 'string=?))))
(define-proc-type gauche make-keyword <keyword> ((or <string> <symbol>)))
(define-proc-type gauche make-list <list> (<integer> (&optional <top>)))
(define-proc-type gauche make-module
  <module> ((or <symbol> #f) (&keyword :if-exists (or :error #f))))
;(define-proc-type gauche make-string-pointer <top> (<top>))
(define-proc-type gauche make-tree-map <tree-map> ((&procedure) (&procedure)))
(define-proc-type gauche make-weak-vector <weak-vector> (<integer>))
(define-proc-type gauche map$ <procedure> ((&procedure)))
(define-proc-type gauche member$ <procedure> ((&procedure)))
;(define-proc-type gauche merge <top> (<top>))
;(define-proc-type gauche merge! <top> (<top>))
;(define-proc-type gauche method-more-specific? <top> (<top>))
(define-proc-type gauche min&max
  (<integer> <integer>) (<integer> (&rest <integer>))
  (<rational> <rational>) (<rational> (&rest <rational>))
  (<real> <real>) (<real> (&rest <real>)))
(define-proc-type gauche modf (<real> <real>) (<real>))
(define-proc-type gauche module-exports <list> (<module>))
(define-proc-type gauche module-imports <list> (<module>))
(define-proc-type gauche module-name <symbol> (<module>))
(define-proc-type gauche module-name->path <string> (<symbol>))
(define-proc-type gauche module-parents <list> (<module>))
(define-proc-type gauche module-precedence-list <list> (<module>))
(define-proc-type gauche module-table <hash-table> (<module>))
(define-proc-type gauche module? (type? module? <module>) (<top>))
;(define-proc-type gauche monotonic-merge <top> (<top>))
(define-proc-type gauche null-list? <boolean> (<top>))

;; TODO: 本当にロードしないようにする
(define-proc-type gauche %autoload
  (lambda (env args)
    (let ((module (list-ref args 0))
          (name (list-ref args 1)))
    (cond
     ((and (is-a? module <value-object>)
           (is-a? name <value-object>))
      (eval (if (symbol? (object-of name))
                `(use ,(object-of name))
                `(load ,(object-of name)))
            (object-of module))
      (value-unknown))
     (else
      (value-unknown)))))
  (<module> (or <string> <symbol>) <list>))
(define-proc-type gauche object-*)
(define-proc-type gauche object-+)
(define-proc-type gauche object--)
(define-proc-type gauche object-/)
(define-proc-type gauche object-apply)
(define-proc-type gauche object-compare)
(define-proc-type gauche object-equal?)
(define-proc-type gauche object-hash)
(define-proc-type gauche object-source-info)
(define-proc-type gauche open-coding-aware-port <port> (<port>))
;(define-proc-type gauche open-input-buffered-port <top> (<top>))
(define-proc-type gauche open-input-fd-port
  <port> (<integer> (&keyword :buffering (or :full
                                             :none
                                             :line
                                             :modest)
                              :name <string>
                              :owner? <boolean>)))
(define-proc-type gauche open-input-string <port> (<string>))
;(define-proc-type gauche open-output-buffered-port <top> (<top>))
(define-proc-type gauche open-output-fd-port
  <port> (<integer> (&keyword :buffering (or :full
                                             :none
                                             :line)
                              :name <string>
                              :owner? <boolean>)))
(define-proc-type gauche open-output-string <port> ((&keyword :private? <boolean>)))
(define-proc-type gauche pa$ <procedure>
  ((&procedure) (&rest <top>)))
(define-proc-type gauche partition$ <procedure> ((&procedure)))
(define-proc-type gauche path->module-name <symbol> (<string>))
(define-proc-type gauche peek-byte (or <integer> <eof-object>) ((&optional <port>)))
;(define-proc-type gauche port->byte-string <top> (<top>))
(define-proc-type gauche port->list <list> ((&procedure) <port>))
(define-proc-type gauche port->sexp-list <list> ((&procedure) <port>))
(define-proc-type gauche port->string <string> (<port>))
(define-proc-type gauche port->string-list <list> (<port>))
(define-proc-type gauche port-buffering (or <keyword> #f) (<port>))
(define-proc-type gauche port-closed? <boolean> (<port>))
(define-proc-type gauche port-current-line <integer> (<port>))
(define-proc-type gauche port-fd-dup! <undefined-object> (<port> <port>))
(define-proc-type gauche port-file-number (or <integer> #f) (<port>))
(define-proc-type gauche port-fold ??? ((&procedure) <top> (&procedure)))
(define-proc-type gauche port-fold-right ??? ((&procedure) <top> (&procedure)))
(define-proc-type gauche port-for-each <undefined-object> ((&procedure) (&procedure)))
(define-proc-type gauche port-map <list> ((&procedure) (&procedure)))
(define-proc-type gauche port-name <string> (<port>))
;(define-proc-type gauche port-position-prefix <top> (<top>))
(define-proc-type gauche port-seek
  (or <integer> #f) (<port> <integer> (&optional <integer>)))
(define-proc-type gauche port-tell (or <integer> #f) (<port>))
(define-proc-type gauche port-type <symbol> (<port>))
(define-proc-type gauche print <undefined-object> ((&rest <top>)))
(define-proc-type gauche procedure-arity-includes? <boolean> ((&procedure) <integer>))
;(define-proc-type gauche procedure-info <top> (<top>))
(define-proc-type gauche profiler-reset <undefined-object> ())
(define-proc-type gauche profiler-show <undefined-object>
  ((&keyword :sort-by (or 'time 'count 'time-per-call)
             :max-rows (or <integer> #f))))
;(define-proc-type gauche profiler-show-load-stats <top> (<top>))
(define-proc-type gauche profiler-start <undefined-object> ())
(define-proc-type gauche profiler-stop <undefined-object> ())
;(define-proc-type gauche promise-kind <top> (<top>))
(define-proc-type gauche promise? (type? promise? <promise>) (<top>))
(define-proc-type gauche proper-list? <boolean> (<top>))
(define-proc-type gauche provide ??? (<string>))
(define-proc-type gauche provided? <boolean> (<string>))
(define-proc-type gauche quotient&remainder (<integer> <integer>) (<integer> <integer>))
(define-proc-type gauche raise ??? (<top>))
(define-proc-type gauche read-block
  (or <string> <eof-object>) (<integer> (&optional <port>)))
(define-proc-type gauche read-byte (or <integer> <eof-object>) ((&optional <port>)))
;(define-proc-type gauche read-char-set ??? (<port>))
(define-proc-type gauche read-eval-print-loop ??? ((&procedure) (&procedure) (&procedure) (&procedure)))
(define-proc-type gauche read-from-string ??? (<string> (&optional <integer> <integer>)))
(define-proc-type gauche read-line (or <string> <eof-object>) ((&optional <port> <boolean>)))
;(define-proc-type gauche read-list ??? (<port>))
;(define-proc-type gauche read-reference-has-value? <top> (<top>))
;(define-proc-type gauche read-reference-value <top> (<top>))
;(define-proc-type gauche read-reference? <top> (<top>))
;(define-proc-type gauche redefine-class! <top> (<top>))
(define-proc-type gauche reduce$ <procedure> ((&procedure) (&optional <top>)))
(define-proc-type gauche reduce-right$ <procedure> ((&procedure) (&optional <top>)))
;(define-proc-type gauche ref <top> (<top>))
;(define-proc-type gauche ref* <top> (<top>))
(define-proc-type gauche regexp->string <string> (<regexp>))
;(define-proc-type gauche regexp-case-fold? <top> (<top>))
;(define-proc-type gauche regexp-compile <top> (<top>))
;(define-proc-type gauche regexp-optimize <top> (<top>))
;(define-proc-type gauche regexp-parse <top> (<top>))
(define-proc-type gauche regexp-quote <string> (<string>))
(define-proc-type gauche regexp-replace
  (lambda (env args)
    (let ((subst (list-ref args 2)))
      (cond
       ((or (value-class-match? env subst (value-object <string>))
            (value-procedure? env subst))
        (value-instance <string>))
       (else
        (print-errmsg "wrong type for argument 3 of ~a: <string> or an applicable object required, but got ~a"
                      regexp-replace
                      (x->string subst))
        (value-unknown)))))
  (<regexp> <string> <top>))
(define-proc-type gauche regexp-replace*
  (lambda (env args)
    (define (check-subst i regexp rest-args)
      (cond
       ((null? rest-args)
        (print-errmsg "no substitution is specified for ~a" regexp)
        (value-unknown))
       ((or (value-class-match? env (car rest-args) (value-object <string>))
            (value-procedure? env (car rest-args)))
        (check-regexp (+ i 1) (cdr rest-args)))
       (else
        (print-errmsg "wrong type for argument ~a of ~a: <string> or an applicable object required, but got ~a"
                      i
                      regexp-replace*
                      (x->string (car rest-args)))
        (value-unknown))))
    (define (check-regexp i rest-args)
      (cond
       ((null? rest-args)
        (value-instance <string>))
       ((value-class-match? env (car rest-args) (value-object <regexp>))
        (check-subst (+ i 1) (car rest-args) (cdr rest-args)))
       (else
        (print-errmsg "wrong type for argument ~a of ~a: <regexp> required, but got ~a"
                      i
                      regexp-replace*
                      (x->string (car rest-args)))
        (value-unknown))))
    (check-regexp 2 (cdr args)))

    (<string> (&rest <top>)))
(define-proc-type gauche regexp-replace-all
  (lambda (env args)
    (let ((subst (list-ref args 2)))
      (cond
       ((or (value-class-match? env subst (value-object <string>))
            (value-procedure? env subst))
        (value-instance <string>))
       (else
        (print-errmsg "wrong type for argument 3 of ~a: <string> or an applicable object required, but got ~a"
                      regexp-replace-all
                      (x->string subst))
        (value-unknown)))))
  (<regexp> <string> <top>))
(define-proc-type gauche regexp-replace-all*
  (lambda (env args)
    (define (check-subst i regexp rest-args)
      (cond
       ((null? rest-args)
        (print-errmsg "no substitution is specified for ~a" regexp)
        (value-unknown))
       ((or (value-class-match? env (car rest-args) (value-object <string>))
            (value-procedure? env (car rest-args)))
        (check-regexp (+ i 1) (cdr rest-args)))
       (else
        (print-errmsg "wrong type for argument ~a of ~a: <string> or an applicable object required, but got ~a"
                      i
                      regexp-replace-all*
                      (x->string (car rest-args)))
        (value-unknown))))
    (define (check-regexp i rest-args)
      (cond
       ((null? rest-args)
        (value-instance <string>))
       ((value-class-match? env (car rest-args) (value-object <regexp>))
        (check-subst (+ i 1) (car rest-args) (cdr rest-args)))
       (else
        (print-errmsg "wrong type for argument ~a of ~a: <regexp> required, but got ~a"
                      i
                      regexp-replace-all*
                      (x->string (car rest-args)))
        (value-unknown))))
    (check-regexp 2 (cdr args)))
  (<string> (&rest <top>)))
(define-proc-type gauche regexp? (type? regexp? <regexp>) (<top>))
(define-proc-type gauche regmatch? (type? regmatch? <regmatch>) (<top>))
(define-proc-type gauche remove$ <procedure> ((&procedure)))
;(define-proc-type gauche report-error <top> (<top>))
(define-proc-type gauche reverse! <list> (<list>))
(define-proc-type gauche round->exact <integer> (<real>))
(define-proc-type gauche rxmatch (or <regmatch> #f) (<regexp> <string>))
(define-proc-type gauche rxmatch-after
  (or <string> #f) ((or <regmatch> #f) (&optional <integer>)))
(define-proc-type gauche rxmatch-before
  (or <string> #f) ((or <regmatch> #f) (&optional <integer>)))
(define-proc-type gauche rxmatch-end
  (or <integer> #f) ((or <regmatch> #f) (&optional <integer>)))
(define-proc-type gauche rxmatch-num-matches (or <integer> #f) ((or <regmatch> #f)))
(define-proc-type gauche rxmatch-start
  (or <integer> #f) ((or <regmatch> #f) (&optional <integer>)))
(define-proc-type gauche rxmatch-substring
  (or <string> #f) ((or <regmatch> #f) (&optional <integer>)))
(define-proc-type gauche seconds->time <time> (<real>))
(define-proc-type gauche set-signal-handler!
  (lambda (env args)
    (cond
     ((or (value-class-match? env (list-ref args 1) (value-object <boolean>))
          (value-procedure? env (list-ref args 1)))
      (value-unknown))
     (else
      (print-errmsg "wrong type for argument 2 of ~a: <boolean> or an applicable object required, but got ~a"
                    set-signal-handler!
                    (list-ref args 1))
      (value-unknown))))
  ((or <integer> <sys-sigset>) <top> (&optional <sys-sigset>)))
(define-proc-type gauche set-signal-pending-limit ??? (<integer>))
;(define-proc-type gauche setter <top> (<top>))
;(define-proc-type gauche setter of object-apply <top> (<top>))
;(define-proc-type gauche setter of ref <top> (<top>))
(define-proc-type gauche sinh <number> (<number>))
;(define-proc-type gauche slot-bound-using-accessor? <top> (<top>))
;(define-proc-type gauche slot-bound-using-class? <top> (<top>))
(define-proc-type gauche slot-bound? <boolean> (<top> <symbol>))
;(define-proc-type gauche slot-definition-accessor <top> (<top>))
;(define-proc-type gauche slot-definition-allocation <top> (<top>))
;(define-proc-type gauche slot-definition-getter <top> (<top>))
(define-proc-type gauche slot-definition-name <symbol> (<top>))
;(define-proc-type gauche slot-definition-option <top> (<top>))
(define-proc-type gauche slot-definition-options <list> (<top>))
;(define-proc-type gauche slot-definition-setter <top> (<top>))
;(define-proc-type gauche slot-exists-using-class? <top> (<top>))
(define-proc-type gauche slot-exists? <boolean> (<top> <symbol>))
;(define-proc-type gauche slot-initialize-using-accessor! <top> (<top>))
;(define-proc-type gauche slot-missing <top> (<top>))
(define-proc-type gauche slot-push! ??? (<top> <symbol> <top>))
(define-proc-type gauche slot-ref ??? (<top> <symbol>))
;(define-proc-type gauche slot-ref-using-accessor <top> (<top>))
;(define-proc-type gauche slot-ref-using-class <top> (<top>))
(define-proc-type gauche slot-set! ??? (<top> <symbol> <top>))
;(define-proc-type gauche slot-set-using-accessor! <top> (<top>))
;(define-proc-type gauche slot-set-using-class! <top> (<top>))
;(define-proc-type gauche slot-unbound <top> (<top>))
(define-proc-type gauche sort
  (lambda (env args)
    (sort-ret-type sort env args))
  (<sequence> (&optional <top>)))
(define-proc-type gauche sort!
  (lambda (env args)
    (sort-ret-type sort! env args))
  (<sequence> (&optional <top>)))
;(define-proc-type gauche sort-applicable-methods <top> (<top>))
;(define-proc-type gauche sorted? <top> (<top>))
(define-proc-type gauche split-at (<list> <list>) (<list> <integer>))
(define-proc-type gauche stable-sort
  (lambda (env args)
    (sort-ret-type stable-sort env args))
  (<sequence> (&procedure)))
(define-proc-type gauche stable-sort!
  (lambda (env args)
    (sort-ret-type stable-sort! env args))
  (<sequence> (&procedure)))
(define-proc-type gauche standard-error-port <port> ())
(define-proc-type gauche standard-input-port <port> ())
(define-proc-type gauche standard-output-port <port> ())
(define-proc-type gauche string->regexp <regexp> (<string> (&keyword :case-fold
                                                                     <boolean>)))
(define-proc-type gauche string-byte-ref <integer> (<string> <integer>))
(define-proc-type gauche string-byte-set! ??? (<string> <integer> <char>))
(define-proc-type gauche string-complete->incomplete <string> (<string>))
(define-proc-type gauche string-fill! <string> (<string> <char> (&optional <integer> <integer>)))
(define-proc-type gauche string-immutable? <boolean> (<top>))
(define-proc-type gauche string-incomplete->complete
  <string> (<string> (&optional (or #f :omit <char>))))
(define-proc-type gauche string-incomplete->complete!
  <string> (<string> (&optional (or #f :omit <char>))))
(define-proc-type gauche string-incomplete? <boolean> (<top>))
;(define-proc-type gauche string-interpolate <top> (<top>))
(define-proc-type gauche string-join
  <string> (<list> (&optional <string> (or 'infix 'strict-infix 'prefix 'suffix))))
;(define-proc-type gauche string-pointer-byte-index <top> (<top>))
;(define-proc-type gauche string-pointer-copy <top> (<top>))
;(define-proc-type gauche string-pointer-index <top> (<top>))
;(define-proc-type gauche string-pointer-next! <top> (<top>))
;(define-proc-type gauche string-pointer-prev! <top> (<top>))
;(define-proc-type gauche string-pointer-ref <top> (<top>))
;(define-proc-type gauche string-pointer-set! <top> (<top>))
;(define-proc-type gauche string-pointer-substring <top> (<top>))
;(define-proc-type gauche string-pointer? <top> (<top>))
(define-proc-type gauche string-scan
  (lambda (env args)
    (let ((opt (list-ref args 2 #f)))
      (cond
       ((is-a? opt <value-object>)
        (case (object-of opt)
          ((index before after)
           (value-union (list (value-instance <string>) (value-object #f))))
          ((before* after* both)
           (multi-value (list (value-union (list (value-instance <string>)
                                                 (value-object #f)))
                              (value-union (list (value-instance <string>)
                                                 (value-object #f))))))
          (else
           (value-unknown))))
       (else
        (value-unknown)))))
  (<string> (or <string> <char>) (&optional (or 'index
                                                'before
                                                'after
                                                'before*
                                                'after*
                                                'both))))
(define-proc-type gauche string-size <integer> (<string>))
(define-proc-type gauche string-split
  (lambda (env args)
    (let ((s (list-ref args 1)))
      (cond
       ((or (value-class-match? env s (value-object <char>))
            (value-class-match? env s (value-object <char-set>))
            (value-class-match? env s (value-object <string>))
            (value-class-match? env s (value-object <regexp>))
            (value-procedure? env s))
        (value-instance <list>))
       (else
        (print-errmsg "wrong type for argument 2 of ~a: <char>, <char-set>, <string>, <regexp> or an applicable object required, but got ~a"
                      string-split
                      (x->string s))
        (value-unknown)))))
  (<string> <top>))
(define-proc-type gauche subr? <boolean> (<top>))
;(define-proc-type gauche supported-character-encoding? <top> (<top>))
;(define-proc-type gauche supported-character-encodings <top> (<top>))
;(define-proc-type gauche symbol-bound? <top> (<top>))
(define-proc-type gauche sys-abort ??? ())
(define-proc-type gauche sys-access <boolean> (<string> <integer>))
(define-proc-type gauche sys-alarm <integer> (<integer>))
(define-proc-type gauche sys-asctime <string> (<sys-tm>))
(define-proc-type gauche sys-basename <string> (<string>))
(define-proc-type gauche sys-chdir <boolean> (<string>))
(define-proc-type gauche sys-chmod <boolean> (<string> <integer>))
(define-proc-type gauche sys-chown ??? (<string> <integer> <integer>))
(define-proc-type gauche sys-close <boolean> (<integer>))
(define-proc-type gauche sys-crypt <string> (<string> <string>))
(define-proc-type gauche sys-ctermid <string> ())
(define-proc-type gauche sys-ctime <string> ((or <time> <real>)))
(define-proc-type gauche sys-difftime
  <real> ((or <time> <real>) (or <time> <real>)))
(define-proc-type gauche sys-dirname <string> (<string>))
(define-proc-type gauche sys-environ <list> ())
(define-proc-type gauche sys-environ->alist <list> (<list>))
(define-proc-type gauche sys-exec
  <undefined-object> (<string> <list> (&keyword :iomap <list>
                                                :sigmask (or <sys-sigset> #f))))
(define-proc-type gauche sys-exit <undefined-object> (<integer>))
(define-proc-type gauche sys-fchmod <boolean> ((or <port> <integer>) <integer>))
(define-proc-type gauche sys-fdset <sys-fdset> ((&rest (or <sys-fdset> <integer>))))
(define-proc-type gauche sys-fdset->list <list> (<sys-fdset>))
(define-proc-type gauche sys-fdset-clear! <sys-fdset> (<sys-fdset>))
(define-proc-type gauche sys-fdset-copy! <sys-fdset> (<sys-fdset> <sys-fdset>))
(define-proc-type gauche sys-fdset-max-fd <integer> (<sys-fdset>))
(define-proc-type gauche sys-fdset-ref <boolean> (<sys-fdset> (or <port> <integer>)))
(define-proc-type gauche sys-fdset-set!
  <undefined-object> (<sys-fdset> (or <port> <integer>) <boolean>))
(define-proc-type gauche sys-fork <integer> ())
(define-proc-type gauche sys-fork-and-exec
  <integer> (<string> <list> (&keyword :iomap <list>
                                       :sigmask (or <sys-sigset> #f))))
(define-proc-type gauche sys-fstat <sys-stat> ((or <port> <integer>)))
(define-proc-type gauche sys-ftruncate <boolean> ((or <port> <integer>) <integer>))
(define-proc-type gauche sys-getcwd <string> ())
(define-proc-type gauche sys-getdomainname <string> ())
(define-proc-type gauche sys-getegid <integer> ())
(define-proc-type gauche sys-getenv (or <string> #f) (<string>))
(define-proc-type gauche sys-geteuid <integer> ())
(define-proc-type gauche sys-getgid <integer> ())
(define-proc-type gauche sys-getgrgid (or <sys-group> #f) (<integer>))
(define-proc-type gauche sys-getgrnam (or <sys-group> #f) (<string>))
(define-proc-type gauche sys-getgroups <list> ())
(define-proc-type gauche sys-gethostname <string> ())
(define-proc-type gauche sys-getloadavg <list> (<integer>))
(define-proc-type gauche sys-getlogin (or <string> #f) ())
(define-proc-type gauche sys-getpgid <integer> (<integer>))
(define-proc-type gauche sys-getpgrp <integer> ())
(define-proc-type gauche sys-getpid <integer> ())
(define-proc-type gauche sys-getppid <integer> ())
(define-proc-type gauche sys-getpwnam (or <sys-passwd> #f) (<string>))
(define-proc-type gauche sys-getpwuid (or <sys-passwd> #f) (<integer>))
(define-proc-type gauche sys-gettimeofday (<integer> <integer>) ())
(define-proc-type gauche sys-getuid <integer> ())
(define-proc-type gauche sys-gid->group-name (or <string> #f) (<integer>))
(define-proc-type gauche sys-glob <list> (<string> (&keyword :separator <char-set>
                                                             :folder <top>)))
(define-proc-type gauche sys-gmtime <sys-tm> ((or <time> <real>)))
(define-proc-type gauche sys-group-name->gid (or <integer> #f) (<string>))
(define-proc-type gauche sys-isatty <boolean> ((or <port> <integer>)))
(define-proc-type gauche sys-kill <integer> (<integer> <integer>))
;(define-proc-type gauche sys-lchown <top> (<top>))
(define-proc-type gauche sys-link <boolean> (<string> <string>))
(define-proc-type gauche sys-localeconv <list> ())
(define-proc-type gauche sys-localtime <sys-tm> ((or <time> <real>)))
(define-proc-type gauche sys-lstat <sys-stat> (<string>))
(define-proc-type gauche sys-mkdir <boolean> (<string> (or <integer> #f)))
(define-proc-type gauche sys-mkfifo <boolean> (<string> (or <integer> #f)))
(define-proc-type gauche sys-mkstemp (<port> <string>) (<string>))
(define-proc-type gauche sys-mktime <integer> (<sys-tm>))
(define-proc-type gauche sys-nanosleep (or <time> #f) ((or <real> <time>)))
(define-proc-type gauche sys-normalize-pathname
  <string> (<string> (&keyword :absolute <boolean>
                               :expand <boolean>
                               :canonicalize <boolean>)))
(define-proc-type gauche sys-pause <undefined-object> ())
(define-proc-type gauche sys-pipe
  (<port> <port>) ((&keyword :buffering (or :full :line :none))))
(define-proc-type gauche sys-putenv ??? (<string> <string>))
(define-proc-type gauche sys-random <integer> ())
(define-proc-type gauche sys-readdir <list> (<string>))
(define-proc-type gauche sys-readlink <string> (<string>))
(define-proc-type gauche sys-realpath <string> (<string>))
(define-proc-type gauche sys-remove <boolean> (<string>))
(define-proc-type gauche sys-rename <boolean> (<string> <string>))
(define-proc-type gauche sys-rmdir <boolean> (<string>))
(define-proc-type gauche sys-select
  (<integer> (or <sys-fdset> #f) (or <sys-fdset> #f) (or <sys-fdset> #f))
  ((or <sys-fdset> #f) (or <sys-fdset> #f) (or <sys-fdset> #f)
   (&optional (or <real> <list>))))
(define-proc-type gauche sys-select!
  (<integer> (or <sys-fdset> #f) (or <sys-fdset> #f) (or <sys-fdset> #f))
  ((or <sys-fdset> #f) (or <sys-fdset> #f) (or <sys-fdset> #f)
   (&optional (or <real> <list>))))
;(define-proc-type gauche sys-setenv <top> (<top>))
(define-proc-type gauche sys-setgid ??? (<integer>))
(define-proc-type gauche sys-setlocale <string> (<integer> <string>))
(define-proc-type gauche sys-setpgid ??? (<integer> <integer>))
(define-proc-type gauche sys-setsid ??? ())
(define-proc-type gauche sys-setuid ??? (<integer>))
(define-proc-type gauche sys-sigmask <sys-sigset> (<integer> (or <sys-sigset> #f)))
(define-proc-type gauche sys-signal-name <string> (<integer>))
(define-proc-type gauche sys-sigset
  <sys-sigset> ((&rest (or <integer> <sys-sigset> #t))))
(define-proc-type gauche sys-sigset-add!
  <sys-sigset> (<sys-sigset> (&rest (or <integer> <sys-sigset> #t))))
(define-proc-type gauche sys-sigset-delete!
  <sys-sigset> (<sys-sigset> (&rest (or <integer> <sys-sigset> #t))))
(define-proc-type gauche sys-sigset-empty! <sys-sigset> (<sys-sigset>))
(define-proc-type gauche sys-sigset-fill! <sys-sigset> (<sys-sigset>))
(define-proc-type gauche sys-sigsuspend <undefined-object> (<sys-sigset>))
(define-proc-type gauche sys-sigwait <integer> (<sys-sigset>))
(define-proc-type gauche sys-sleep <integer> (<integer>))
(define-proc-type gauche sys-srandom <integer> (<integer>))
(define-proc-type gauche sys-stat <sys-stat> (<string>))
;(define-proc-type gauche sys-stat->atime <top> (<top>))
;(define-proc-type gauche sys-stat->ctime <top> (<top>))
;(define-proc-type gauche sys-stat->dev <top> (<top>))
;(define-proc-type gauche sys-stat->file-type <top> (<top>))
;(define-proc-type gauche sys-stat->gid <top> (<top>))
;(define-proc-type gauche sys-stat->ino <top> (<top>))
;(define-proc-type gauche sys-stat->mode <top> (<top>))
;(define-proc-type gauche sys-stat->mtime <top> (<top>))
;(define-proc-type gauche sys-stat->nlink <top> (<top>))
;(define-proc-type gauche sys-stat->rdev <top> (<top>))
;(define-proc-type gauche sys-stat->size <top> (<top>))
;(define-proc-type gauche sys-stat->type <top> (<top>))
;(define-proc-type gauche sys-stat->uid <top> (<top>))
(define-proc-type gauche sys-strerror <string> (<integer>))
(define-proc-type gauche sys-strftime <string> (<string> <sys-tm>))
(define-proc-type gauche sys-symlink <boolean> (<string> <string>))
(define-proc-type gauche sys-system <integer> (<string>))
(define-proc-type gauche sys-time <real> ())
(define-proc-type gauche sys-times <list> ())
(define-proc-type gauche sys-tm->alist <list> (<sys-tm>))
(define-proc-type gauche sys-tmpnam <string> ())
(define-proc-type gauche sys-truncate <boolean> (<string> <integer>))
(define-proc-type gauche sys-ttyname (or <string> #f) ((or <port> <integer>)))
(define-proc-type gauche sys-uid->user-name (or <string> #f) (<integer>))
(define-proc-type gauche sys-umask <integer> ((&optional (or <integer> #f))))
(define-proc-type gauche sys-uname <list> ())
(define-proc-type gauche sys-unlink <boolean> (<string>))
;(define-proc-type gauche sys-unsetenv <top> (<top>))
(define-proc-type gauche sys-user-name->uid (or <integer> #f) (<string>))
(define-proc-type gauche sys-utime
  <undefined-object> (<string> (&optional <real> <real>)))
(define-proc-type gauche sys-wait (<integer> <integer>) ())
(define-proc-type gauche sys-wait-exit-status <integer> (<integer>))
(define-proc-type gauche sys-wait-exited? <boolean> (<integer>))
(define-proc-type gauche sys-wait-signaled? <boolean> (<integer>))
(define-proc-type gauche sys-wait-stopped? <boolean> (<integer>))
(define-proc-type gauche sys-wait-stopsig <integer> (<integer>))
(define-proc-type gauche sys-wait-termsig <integer> (<integer>))
(define-proc-type gauche sys-waitpid
  (<integer> <integer>) (<integer> (&keyword :nohang <boolean>
                                             :untraced <boolean>)))
(define-proc-type gauche tanh <number> (<number>))
(define-proc-type gauche time->seconds <real> (<time>))
(define-proc-type gauche time? (type? time? <time>) (<top>))
;; (define-proc-type gauche toplevel-closure? <top> (<top>))
;; (define-proc-type gauche touch-instance! <top> (<top>))
(define-proc-type gauche tree-map->alist <list> (<tree-map>))
(define-proc-type gauche tree-map-copy <tree-map> (<tree-map>))
(define-proc-type gauche tree-map-delete! <boolean> (<tree-map> <top>))
(define-proc-type gauche tree-map-empty? <boolean> (<tree-map>))
(define-proc-type gauche tree-map-exists? <boolean> (<tree-map> <top>))
(define-proc-type gauche tree-map-fold ??? (<tree-map> (&procedure) <top>))
(define-proc-type gauche tree-map-fold-right ??? (<tree-map> (&procedure) <top>))
(define-proc-type gauche tree-map-get ??? (<tree-map> <top> (&optional <top>)))
(define-proc-type gauche tree-map-keys <list> (<tree-map>))
(define-proc-type gauche tree-map-max ??? (<tree-map>))
(define-proc-type gauche tree-map-min ??? (<tree-map>))
(define-proc-type gauche tree-map-num-entries <integer> (<tree-map>))
(define-proc-type gauche tree-map-pop! ??? (<tree-map> <top> (&optional <top>)))
(define-proc-type gauche tree-map-pop-max! ??? (<tree-map>))
(define-proc-type gauche tree-map-pop-min! ??? (<tree-map>))
(define-proc-type gauche tree-map-push! ??? (<tree-map> <top> <top>))
(define-proc-type gauche tree-map-put! ??? (<tree-map> <top> <top>))
(define-proc-type gauche tree-map-update!
  ??? (<tree-map> <top> (&procedure) (&optional <top>)))
(define-proc-type gauche tree-map-values <list> (<tree-map>))
(define-proc-type gauche tree-map? (type? tree-map? <tree-map>) (<top>))
;; (define-proc-type gauche truncate->exact <top> (<top>))
(define-proc-type gauche ucs->char (or <char> #f) (<integer>))
(define-proc-type gauche undefined <undefined-object> ())
(define-proc-type gauche undefined? (type? undefined? <undefined-object>) (<top>))
(define-proc-type gauche unwrap-syntax ??? (<top>))
;; (define-proc-type gauche update-direct-method! <top> (<top>))
;; (define-proc-type gauche update-direct-subclass! <top> (<top>))
(define-proc-type gauche vector-copy <vector> (<vector> (&optional <integer> <integer> <top>)))
;; (define-proc-type gauche vm-dump <top> (<top>))
;; (define-proc-type gauche vm-get-stack-trace <top> (<top>))
;; (define-proc-type gauche vm-get-stack-trace-lite <top> (<top>))
;; (define-proc-type gauche vm-set-default-exception-handler <top> (<top>))
;; (define-proc-type gauche warn <top> (<top>))
(define-proc-type gauche weak-vector-length <integer> (<weak-vector>))
(define-proc-type gauche weak-vector-ref
  ??? (<weak-vector> <integer> (&optional <top>)))
(define-proc-type gauche weak-vector-set! ??? (<weak-vector> <integer> <top>))
(define-proc-type gauche with-error-handler
  ??? ((&procedure) (&procedure) (&keyword :rewind-before <boolean>)))
(define-proc-type gauche with-error-to-port ??? (<port> (&procedure)))
(define-proc-type gauche with-exception-handler ??? ((&procedure) (&procedure)))
(define-proc-type gauche with-input-from-port ??? (<port> (&procedure)))
(define-proc-type gauche with-input-from-string ??? (<string> (&procedure)))
(define-proc-type gauche with-output-to-port ??? (<port> (&procedure)))
(define-proc-type gauche with-output-to-string <string> ((&procedure)))
(define-proc-type gauche with-port-locking ??? (<port> (&procedure)))
(define-proc-type gauche with-ports
  ??? ((or <port> #f) (or <port> #f) (or <port> #f) (&procedure)))
(define-proc-type gauche with-string-io <string> (<string> (&procedure)))
(define-proc-type gauche write* ??? (<top> (&optional <port>)))
(define-proc-type gauche write-byte ??? (<integer> (&optional <port>)))
;; (define-proc-type gauche write-limited <top> (<top>))
(define-proc-type gauche write-object ??? (<top> (&optional <port>)))
(define-proc-type gauche write-to-string
  (lambda (env args)
    (let ((writer (list-ref args 1 #f)))
      (cond
       ((not writer)
        (value-instance <string>))
       ((and writer (value-procedure? env writer))
        (value-instance <string>))
       (else
        (print-errmsg "wrong type for argument 2 of ~a: an applicable object required, but got ~a"
                      write-to-string
                      (x->string writer))
        (value-unknown)))))
  (<top> (&optional <top>)))
(define-proc-type gauche write-with-shared-structure ??? (<top> (&optional <port>)))
(define-proc-type gauche x->integer <integer> (<top>))
(define-proc-type gauche x->number <number> (<top>))
(define-proc-type gauche x->string <string> (<top>))

;; (hash-table-for-each
;;  (module-table (find-module 'scheme))
;;  (lambda (k v)
;;    (let ((obj (global-variable-ref (find-module 'scheme) k)))
;;    (if (and (#/^[^%]/ (symbol->string k)) (procedure? obj))
;;        (format #t "(define-proc-type scheme ~a unknown (~a))~%"
;;                k
;;                (string-join (let ((a (arity obj)))
;;                               (if (arity-at-least? a)
;;                                   (append (make-list (arity-at-least-value a)
;;                                                      "<top>")
;;                                           '("..."))
;;                                   (make-list a "<top>")))))))))
                      

(provide "codecheck/environment")

;; end of file
