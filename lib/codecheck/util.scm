;; -*- coding: utf-8; mode: scheme -*-
;;
;; util.scm - Utilities for codecheck
;;
;;   Copyright (c) 2008 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define-module codecheck.util
  (use gauche.parameter)
  (use gauche.sequence)
  (use util.queue)
  
  (export src-info
          with-src-info
          with-src
          make-src-info

          with-pool-error-message
          print-msg
          print-errmsg
          print-warnmsg

          push-call-stack!
          pop-call-stack!
          get-call-stack
          current-call-stack

          id-module
          id-symbol
          
          lvar-id
          lvar-modified?

          vm-current-module
          vm-compiler-flag-is-set?
          vm-compiler-flag-set!
          vm-compiler-flag-clear!
          SCM_COMPILE_NOINLINE_GLOBALS

          pass1
          ))

(select-module codecheck.util)

(define src-info (make-parameter #f))

(define-syntax with-src-info
  (syntax-rules ()
    ((_ sinf body ...)
     (parameterize ((src-info (or sinf (src-info))))
       body ...))))

(define-syntax with-src
  (syntax-rules ()
    ((_ expr body ...)
     (parameterize ((src-info (or (and (pair? expr)
                                       (debug-source-info expr))
                                  (src-info))))
       body ...))))

(define (make-src-info filename line-no)
  (list filename line-no))

(define message-queue (make-parameter #f))

(define (with-pool-error-message proc msg-printer)
  (let ((result #f)
        (lst #f))
    (parameterize ((message-queue (make-queue)))
      (set! result (proc))
      (set! lst (dequeue-all! (message-queue))))
    (msg-printer result lst)
    result))

(define (print-msg type sinf msg)
  (if (message-queue)
      (enqueue! (message-queue) (list type sinf msg))
      (display (format "~a:~d: ~a: ~a\n" (car sinf) (cadr sinf) type msg))))

(define (print-errmsg fmt . args)
  (let* ((sinf (or (src-info) (list '- '-)))
         (msg (apply format fmt args)))
    (print-msg 'error sinf msg)))

(define (print-warnmsg fmt . args)
  (let* ((sinf (or (src-info) (list '- '-)))
         (msg (apply format fmt args)))
    (print-msg 'warning sinf msg)))

;; identifier utilities
(define (id-module id)
  (module-name (slot-ref id 'module)))

(define (id-symbol id)
  (identifier->symbol id))

(define-macro (import-define mod . name-list)
  `(begin
     ,@(map (lambda (name)
              `(define ,name (with-module ,mod ,name)))
            name-list)))

;; lvar utilities
(define (lvar-id lvar)
  (let ((id (vector-ref lvar 1)))
    (if (or (identifier? id) (symbol? id))
        id
        (begin
          (print-errmsg "variable required, but got: ~s" id)
          #f))))

(define (lvar-modified? lvar)
  (not (= (vector-ref lvar 4) 0)))


(import-define gauche.internal
               vm-compiler-flag-is-set?
               vm-compiler-flag-set!
               vm-compiler-flag-clear!
               SCM_COMPILE_NOINLINE_GLOBALS
               vm-current-module)

(define (pass1 expr cenv)
  (let ((flag (vm-compiler-flag-is-set? SCM_COMPILE_NOINLINE_GLOBALS)))
    (dynamic-wind
        (lambda ()
          (vm-compiler-flag-set! SCM_COMPILE_NOINLINE_GLOBALS))
        (lambda ()
          ((with-module gauche.internal pass1) expr cenv))
        (lambda ()
          (unless flag
            (vm-compiler-flag-clear! SCM_COMPILE_NOINLINE_GLOBALS))))))

(provide "codecheck/util")

;; end of file
